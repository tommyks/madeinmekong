﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(main))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tsb_refresh = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.tsb_food = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsb_types = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsb_unit = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsb_tables = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbs_employees = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsb_stocks = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.tsb_reportin = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsb_reportstocks = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsb_username = New System.Windows.Forms.ToolStripDropDownButton()
        Me.tsb_changepassword = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsb_lock = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsb_exit = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.gb_changetables = New System.Windows.Forms.GroupBox()
        Me.btn_closechangetable = New System.Windows.Forms.Button()
        Me.btn_changetb = New System.Windows.Forms.Button()
        Me.cbx_changetables = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dg_total = New System.Windows.Forms.DataGridView()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lab_datetime = New System.Windows.Forms.Label()
        Me.lab_total = New System.Windows.Forms.Label()
        Me.ToolStrip3 = New System.Windows.Forms.ToolStrip()
        Me.btn_changetables = New System.Windows.Forms.ToolStripButton()
        Me.btn_checkout = New System.Windows.Forms.ToolStripButton()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dg_ordersfood = New System.Windows.Forms.DataGridView()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.dg_food = New System.Windows.Forms.DataGridView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btn_order = New System.Windows.Forms.Button()
        Me.lb_foodname = New System.Windows.Forms.Label()
        Me.txt_qty = New System.Windows.Forms.TextBox()
        Me.ToolStrip4 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.txt_searchfood = New System.Windows.Forms.ToolStripTextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbx_tables = New System.Windows.Forms.ComboBox()
        Me.txt_billno = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lab_times = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.dg_tablestate = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_search = New System.Windows.Forms.TextBox()
        Me.chk_tables = New System.Windows.Forms.CheckBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.gb_changetables.SuspendLayout()
        CType(Me.dg_total, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.ToolStrip3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dg_ordersfood, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.dg_food, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.ToolStrip4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dg_tablestate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(22, 22)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsb_refresh, Me.ToolStripButton1, Me.tsb_stocks, Me.ToolStripDropDownButton1, Me.tsb_username})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(800, 32)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tsb_refresh
        '
        Me.tsb_refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_refresh.Image = CType(resources.GetObject("tsb_refresh.Image"), System.Drawing.Image)
        Me.tsb_refresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_refresh.Name = "tsb_refresh"
        Me.tsb_refresh.Size = New System.Drawing.Size(26, 29)
        Me.tsb_refresh.Text = "ToolStripButton2"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsb_food, Me.tsb_types, Me.tsb_unit, Me.tsb_tables, Me.ToolStripSeparator2, Me.tbs_employees})
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(100, 29)
        Me.ToolStripButton1.Text = "ຈັດການຂໍ້ມູນ"
        '
        'tsb_food
        '
        Me.tsb_food.Name = "tsb_food"
        Me.tsb_food.Size = New System.Drawing.Size(145, 28)
        Me.tsb_food.Text = "ອາຫານ"
        '
        'tsb_types
        '
        Me.tsb_types.Name = "tsb_types"
        Me.tsb_types.Size = New System.Drawing.Size(145, 28)
        Me.tsb_types.Text = "ປະເພດ"
        '
        'tsb_unit
        '
        Me.tsb_unit.Name = "tsb_unit"
        Me.tsb_unit.Size = New System.Drawing.Size(145, 28)
        Me.tsb_unit.Text = "ຫົວໜ່ວຍ"
        '
        'tsb_tables
        '
        Me.tsb_tables.Name = "tsb_tables"
        Me.tsb_tables.Size = New System.Drawing.Size(145, 28)
        Me.tsb_tables.Text = "ໂຕະ"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(142, 6)
        '
        'tbs_employees
        '
        Me.tbs_employees.Name = "tbs_employees"
        Me.tbs_employees.Size = New System.Drawing.Size(145, 28)
        Me.tbs_employees.Text = "ພະນັກງານ"
        '
        'tsb_stocks
        '
        Me.tsb_stocks.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsb_stocks.Image = CType(resources.GetObject("tsb_stocks.Image"), System.Drawing.Image)
        Me.tsb_stocks.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_stocks.Name = "tsb_stocks"
        Me.tsb_stocks.Size = New System.Drawing.Size(72, 29)
        Me.tsb_stocks.Text = "ສ່າງສິນຄ້າ"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsb_reportin, Me.tsb_reportstocks})
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(76, 29)
        Me.ToolStripDropDownButton1.Text = "ລາຍງານ "
        '
        'tsb_reportin
        '
        Me.tsb_reportin.Name = "tsb_reportin"
        Me.tsb_reportin.Size = New System.Drawing.Size(163, 28)
        Me.tsb_reportin.Text = "ລາຍຮັບ"
        '
        'tsb_reportstocks
        '
        Me.tsb_reportstocks.Name = "tsb_reportstocks"
        Me.tsb_reportstocks.Size = New System.Drawing.Size(163, 28)
        Me.tsb_reportstocks.Text = "ສິນຄ້າໃນສ່າງ"
        '
        'tsb_username
        '
        Me.tsb_username.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.tsb_username.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsb_changepassword, Me.tsb_lock, Me.ToolStripSeparator1, Me.tsb_exit})
        Me.tsb_username.Image = CType(resources.GetObject("tsb_username.Image"), System.Drawing.Image)
        Me.tsb_username.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_username.Name = "tsb_username"
        Me.tsb_username.RightToLeftAutoMirrorImage = True
        Me.tsb_username.Size = New System.Drawing.Size(116, 29)
        Me.tsb_username.Text = "Username"
        '
        'tsb_changepassword
        '
        Me.tsb_changepassword.Image = Global.madeinmekong.My.Resources.Resources.baseline_vpn_key_black_18dp
        Me.tsb_changepassword.Name = "tsb_changepassword"
        Me.tsb_changepassword.Size = New System.Drawing.Size(220, 28)
        Me.tsb_changepassword.Text = "Change Password"
        '
        'tsb_lock
        '
        Me.tsb_lock.Image = Global.madeinmekong.My.Resources.Resources.baseline_lock_black_18dp
        Me.tsb_lock.Name = "tsb_lock"
        Me.tsb_lock.Size = New System.Drawing.Size(220, 28)
        Me.tsb_lock.Text = "Lock"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(217, 6)
        '
        'tsb_exit
        '
        Me.tsb_exit.Image = Global.madeinmekong.My.Resources.Resources.baseline_exit_to_app_black_18dp
        Me.tsb_exit.Name = "tsb_exit"
        Me.tsb_exit.Size = New System.Drawing.Size(220, 28)
        Me.tsb_exit.Text = "Exit"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(536, 568)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ຮັບເມນູ"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox3, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 62)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(530, 503)
        Me.TableLayoutPanel1.TabIndex = 9
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.gb_changetables)
        Me.GroupBox3.Controls.Add(Me.dg_total)
        Me.GroupBox3.Controls.Add(Me.GroupBox5)
        Me.GroupBox3.Controls.Add(Me.ToolStrip3)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox3.Location = New System.Drawing.Point(268, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(259, 497)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "ລາຍການອາຫານ"
        '
        'gb_changetables
        '
        Me.gb_changetables.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gb_changetables.Controls.Add(Me.btn_closechangetable)
        Me.gb_changetables.Controls.Add(Me.btn_changetb)
        Me.gb_changetables.Controls.Add(Me.cbx_changetables)
        Me.gb_changetables.Controls.Add(Me.Label4)
        Me.gb_changetables.Location = New System.Drawing.Point(0, 56)
        Me.gb_changetables.Name = "gb_changetables"
        Me.gb_changetables.Size = New System.Drawing.Size(259, 100)
        Me.gb_changetables.TabIndex = 3
        Me.gb_changetables.TabStop = False
        Me.gb_changetables.Text = "ຍ້າຍໂຕະ"
        Me.gb_changetables.Visible = False
        '
        'btn_closechangetable
        '
        Me.btn_closechangetable.Location = New System.Drawing.Point(171, 61)
        Me.btn_closechangetable.Name = "btn_closechangetable"
        Me.btn_closechangetable.Size = New System.Drawing.Size(78, 31)
        Me.btn_closechangetable.TabIndex = 10
        Me.btn_closechangetable.Text = "ປີດ"
        Me.btn_closechangetable.UseVisualStyleBackColor = True
        '
        'btn_changetb
        '
        Me.btn_changetb.Location = New System.Drawing.Point(6, 61)
        Me.btn_changetb.Name = "btn_changetb"
        Me.btn_changetb.Size = New System.Drawing.Size(159, 31)
        Me.btn_changetb.TabIndex = 9
        Me.btn_changetb.Text = "ຕົກລົງ"
        Me.btn_changetb.UseVisualStyleBackColor = True
        '
        'cbx_changetables
        '
        Me.cbx_changetables.FormattingEnabled = True
        Me.cbx_changetables.Location = New System.Drawing.Point(90, 24)
        Me.cbx_changetables.Name = "cbx_changetables"
        Me.cbx_changetables.Size = New System.Drawing.Size(159, 31)
        Me.cbx_changetables.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 23)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "ຍ້າຍໄປໂຕະ"
        '
        'dg_total
        '
        Me.dg_total.AllowUserToAddRows = False
        Me.dg_total.AllowUserToDeleteRows = False
        Me.dg_total.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_total.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_total.Location = New System.Drawing.Point(3, 56)
        Me.dg_total.Name = "dg_total"
        Me.dg_total.ReadOnly = True
        Me.dg_total.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dg_total.RowTemplate.Height = 25
        Me.dg_total.Size = New System.Drawing.Size(253, 345)
        Me.dg_total.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lab_datetime)
        Me.GroupBox5.Controls.Add(Me.lab_total)
        Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox5.Location = New System.Drawing.Point(3, 401)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(253, 93)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "ລວມຍອດ"
        '
        'lab_datetime
        '
        Me.lab_datetime.AutoSize = True
        Me.lab_datetime.Dock = System.Windows.Forms.DockStyle.Top
        Me.lab_datetime.Location = New System.Drawing.Point(3, 66)
        Me.lab_datetime.Name = "lab_datetime"
        Me.lab_datetime.Size = New System.Drawing.Size(19, 23)
        Me.lab_datetime.TabIndex = 1
        Me.lab_datetime.Text = "0"
        '
        'lab_total
        '
        Me.lab_total.AutoSize = True
        Me.lab_total.Dock = System.Windows.Forms.DockStyle.Top
        Me.lab_total.Font = New System.Drawing.Font("Phetsarath OT", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_total.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lab_total.Location = New System.Drawing.Point(3, 27)
        Me.lab_total.Name = "lab_total"
        Me.lab_total.Size = New System.Drawing.Size(31, 39)
        Me.lab_total.TabIndex = 0
        Me.lab_total.Text = "0"
        Me.lab_total.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ToolStrip3
        '
        Me.ToolStrip3.Font = New System.Drawing.Font("Phetsarath OT", 12.0!)
        Me.ToolStrip3.ImageScalingSize = New System.Drawing.Size(22, 22)
        Me.ToolStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btn_changetables, Me.btn_checkout})
        Me.ToolStrip3.Location = New System.Drawing.Point(3, 27)
        Me.ToolStrip3.Name = "ToolStrip3"
        Me.ToolStrip3.Size = New System.Drawing.Size(253, 29)
        Me.ToolStrip3.TabIndex = 1
        Me.ToolStrip3.Text = "ToolStrip3"
        '
        'btn_changetables
        '
        Me.btn_changetables.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btn_changetables.Image = CType(resources.GetObject("btn_changetables.Image"), System.Drawing.Image)
        Me.btn_changetables.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btn_changetables.Name = "btn_changetables"
        Me.btn_changetables.Size = New System.Drawing.Size(26, 26)
        Me.btn_changetables.Text = "ChangTable"
        '
        'btn_checkout
        '
        Me.btn_checkout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btn_checkout.Image = CType(resources.GetObject("btn_checkout.Image"), System.Drawing.Image)
        Me.btn_checkout.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btn_checkout.Name = "btn_checkout"
        Me.btn_checkout.Size = New System.Drawing.Size(26, 26)
        Me.btn_checkout.Text = "CheckBill"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox4, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox6, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(259, 497)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dg_ordersfood)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(3, 251)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(253, 243)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "ປະຫວັດການສັ່ງອາຫານ"
        '
        'dg_ordersfood
        '
        Me.dg_ordersfood.AllowUserToAddRows = False
        Me.dg_ordersfood.AllowUserToDeleteRows = False
        Me.dg_ordersfood.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_ordersfood.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_ordersfood.Location = New System.Drawing.Point(3, 27)
        Me.dg_ordersfood.Name = "dg_ordersfood"
        Me.dg_ordersfood.ReadOnly = True
        Me.dg_ordersfood.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dg_ordersfood.RowTemplate.Height = 25
        Me.dg_ordersfood.Size = New System.Drawing.Size(247, 213)
        Me.dg_ordersfood.TabIndex = 0
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.dg_food)
        Me.GroupBox6.Controls.Add(Me.Panel3)
        Me.GroupBox6.Controls.Add(Me.ToolStrip4)
        Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox6.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(253, 242)
        Me.GroupBox6.TabIndex = 3
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "ສັ່ງອາຫານ"
        '
        'dg_food
        '
        Me.dg_food.AllowUserToAddRows = False
        Me.dg_food.AllowUserToDeleteRows = False
        Me.dg_food.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_food.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_food.Location = New System.Drawing.Point(3, 59)
        Me.dg_food.Name = "dg_food"
        Me.dg_food.ReadOnly = True
        Me.dg_food.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dg_food.RowTemplate.Height = 25
        Me.dg_food.Size = New System.Drawing.Size(247, 113)
        Me.dg_food.TabIndex = 10
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btn_order)
        Me.Panel3.Controls.Add(Me.lb_foodname)
        Me.Panel3.Controls.Add(Me.txt_qty)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(3, 172)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(247, 67)
        Me.Panel3.TabIndex = 11
        '
        'btn_order
        '
        Me.btn_order.Location = New System.Drawing.Point(144, 29)
        Me.btn_order.Name = "btn_order"
        Me.btn_order.Size = New System.Drawing.Size(92, 31)
        Me.btn_order.TabIndex = 6
        Me.btn_order.Text = "ສັ່ງອາຫານ"
        Me.btn_order.UseVisualStyleBackColor = True
        '
        'lb_foodname
        '
        Me.lb_foodname.AutoSize = True
        Me.lb_foodname.Location = New System.Drawing.Point(6, 3)
        Me.lb_foodname.Name = "lb_foodname"
        Me.lb_foodname.Size = New System.Drawing.Size(0, 23)
        Me.lb_foodname.TabIndex = 5
        '
        'txt_qty
        '
        Me.txt_qty.Location = New System.Drawing.Point(6, 29)
        Me.txt_qty.Name = "txt_qty"
        Me.txt_qty.Size = New System.Drawing.Size(132, 31)
        Me.txt_qty.TabIndex = 4
        '
        'ToolStrip4
        '
        Me.ToolStrip4.AutoSize = False
        Me.ToolStrip4.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip4.ImageScalingSize = New System.Drawing.Size(22, 22)
        Me.ToolStrip4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel2, Me.txt_searchfood})
        Me.ToolStrip4.Location = New System.Drawing.Point(3, 27)
        Me.ToolStrip4.Name = "ToolStrip4"
        Me.ToolStrip4.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.ToolStrip4.Size = New System.Drawing.Size(247, 32)
        Me.ToolStrip4.TabIndex = 9
        Me.ToolStrip4.Text = "ToolStrip4"
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripLabel2.Image = Global.madeinmekong.My.Resources.Resources.outline_search_black_18dp
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(22, 29)
        Me.ToolStripLabel2.Text = "ToolStripLabel2"
        '
        'txt_searchfood
        '
        Me.txt_searchfood.AutoSize = False
        Me.txt_searchfood.Name = "txt_searchfood"
        Me.txt_searchfood.Size = New System.Drawing.Size(170, 32)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cbx_tables)
        Me.Panel1.Controls.Add(Me.txt_billno)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.lab_times)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 27)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(530, 35)
        Me.Panel1.TabIndex = 6
        '
        'cbx_tables
        '
        Me.cbx_tables.FormattingEnabled = True
        Me.cbx_tables.Location = New System.Drawing.Point(44, 2)
        Me.cbx_tables.Name = "cbx_tables"
        Me.cbx_tables.Size = New System.Drawing.Size(159, 31)
        Me.cbx_tables.TabIndex = 6
        '
        'txt_billno
        '
        Me.txt_billno.Enabled = False
        Me.txt_billno.Location = New System.Drawing.Point(245, 2)
        Me.txt_billno.Name = "txt_billno"
        Me.txt_billno.Size = New System.Drawing.Size(207, 31)
        Me.txt_billno.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ໂຕະ"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(209, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "ບີນ"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(458, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 23)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "ເວລາ"
        '
        'lab_times
        '
        Me.lab_times.AutoSize = True
        Me.lab_times.Location = New System.Drawing.Point(505, 5)
        Me.lab_times.Name = "lab_times"
        Me.lab_times.Size = New System.Drawing.Size(87, 23)
        Me.lab_times.TabIndex = 5
        Me.lab_times.Text = "HH:MM SS"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dg_tablestate)
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Right
        Me.GroupBox2.Location = New System.Drawing.Point(536, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(264, 568)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "ໂຕະ"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(0, 0)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(363, 466)
        Me.RichTextBox1.TabIndex = 4
        Me.RichTextBox1.Text = resources.GetString("RichTextBox1.Text")
        Me.RichTextBox1.Visible = False
        '
        'dg_tablestate
        '
        Me.dg_tablestate.AllowUserToAddRows = False
        Me.dg_tablestate.AllowUserToDeleteRows = False
        Me.dg_tablestate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_tablestate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_tablestate.Location = New System.Drawing.Point(3, 99)
        Me.dg_tablestate.Name = "dg_tablestate"
        Me.dg_tablestate.ReadOnly = True
        Me.dg_tablestate.Size = New System.Drawing.Size(258, 466)
        Me.dg_tablestate.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txt_search)
        Me.Panel2.Controls.Add(Me.chk_tables)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 27)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(258, 72)
        Me.Panel2.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 23)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "ຄົ້ນຫາ"
        '
        'txt_search
        '
        Me.txt_search.Location = New System.Drawing.Point(3, 31)
        Me.txt_search.Name = "txt_search"
        Me.txt_search.Size = New System.Drawing.Size(142, 31)
        Me.txt_search.TabIndex = 1
        '
        'chk_tables
        '
        Me.chk_tables.AutoSize = True
        Me.chk_tables.Location = New System.Drawing.Point(71, 4)
        Me.chk_tables.Name = "chk_tables"
        Me.chk_tables.Size = New System.Drawing.Size(74, 27)
        Me.chk_tables.TabIndex = 0
        Me.chk_tables.Text = "ໂຕະວ່າງ"
        Me.chk_tables.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'PrintDocument1
        '
        '
        'main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 23.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 600)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Font = New System.Drawing.Font("Phetsarath OT", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "main"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gb_changetables.ResumeLayout(False)
        Me.gb_changetables.PerformLayout()
        CType(Me.dg_total, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ToolStrip3.ResumeLayout(False)
        Me.ToolStrip3.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dg_ordersfood, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.dg_food, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ToolStrip4.ResumeLayout(False)
        Me.ToolStrip4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dg_tablestate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripDropDownButton1 As ToolStripDropDownButton
    Friend WithEvents tsb_username As ToolStripDropDownButton
    Friend WithEvents tsb_changepassword As ToolStripMenuItem
    Friend WithEvents tsb_lock As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents tsb_exit As ToolStripMenuItem
    Friend WithEvents ToolStripButton1 As ToolStripDropDownButton
    Friend WithEvents tsb_food As ToolStripMenuItem
    Friend WithEvents tsb_types As ToolStripMenuItem
    Friend WithEvents tsb_unit As ToolStripMenuItem
    Friend WithEvents tsb_stocks As ToolStripButton
    Friend WithEvents tsb_reportin As ToolStripMenuItem
    Friend WithEvents tsb_reportstocks As ToolStripMenuItem
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lab_times As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_billno As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents tsb_tables As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents tbs_employees As ToolStripMenuItem
    Friend WithEvents dg_ordersfood As DataGridView
    Friend WithEvents cbx_tables As ComboBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txt_search As TextBox
    Friend WithEvents chk_tables As CheckBox
    Friend WithEvents Label5 As Label
    Friend WithEvents dg_tablestate As DataGridView
    Friend WithEvents tsb_refresh As ToolStripButton
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents dg_total As DataGridView
    Friend WithEvents ToolStrip3 As ToolStrip
    Friend WithEvents btn_checkout As ToolStripButton
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents btn_changetables As ToolStripButton
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents ToolStrip4 As ToolStrip
    Friend WithEvents ToolStripLabel2 As ToolStripLabel
    Friend WithEvents txt_searchfood As ToolStripTextBox
    Friend WithEvents dg_food As DataGridView
    Friend WithEvents Panel3 As Panel
    Friend WithEvents txt_qty As TextBox
    Friend WithEvents btn_order As Button
    Friend WithEvents lb_foodname As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents lab_datetime As Label
    Friend WithEvents lab_total As Label
    Friend WithEvents gb_changetables As GroupBox
    Friend WithEvents cbx_changetables As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_changetb As Button
    Friend WithEvents btn_closechangetable As Button
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents RichTextBox1 As RichTextBox
End Class
