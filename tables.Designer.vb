﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class tables
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(tables))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tsm_del = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.tsb_del = New System.Windows.Forms.ToolStripButton()
        Me.tsb_edit = New System.Windows.Forms.ToolStripButton()
        Me.tsb_close = New System.Windows.Forms.ToolStripButton()
        Me.tsb_add = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.gb_add = New System.Windows.Forms.GroupBox()
        Me.chk_enabled = New System.Windows.Forms.CheckBox()
        Me.btn_closeadd = New System.Windows.Forms.Button()
        Me.btn_store = New System.Windows.Forms.Button()
        Me.txt_tablesname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dg_data = New System.Windows.Forms.DataGridView()
        Me.ctm_dg = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsm_edit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsl_id = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsl_status = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.txt_search = New System.Windows.Forms.ToolStripTextBox()
        Me.GroupBox2.SuspendLayout()
        Me.gb_add.SuspendLayout()
        CType(Me.dg_data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctm_dg.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tsm_del
        '
        Me.tsm_del.Font = New System.Drawing.Font("Phetsarath OT", 12.0!)
        Me.tsm_del.Image = Global.madeinmekong.My.Resources.Resources.baseline_cancel_black_18dp
        Me.tsm_del.Name = "tsm_del"
        Me.tsm_del.Size = New System.Drawing.Size(104, 28)
        Me.tsm_del.Text = "Del"
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripLabel2.Image = Global.madeinmekong.My.Resources.Resources.outline_search_black_18dp
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(22, 29)
        Me.ToolStripLabel2.Text = "ToolStripLabel2"
        '
        'tsb_del
        '
        Me.tsb_del.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_del.Image = Global.madeinmekong.My.Resources.Resources.baseline_cancel_black_18dp
        Me.tsb_del.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_del.Name = "tsb_del"
        Me.tsb_del.Size = New System.Drawing.Size(26, 29)
        Me.tsb_del.Text = "ToolStripButton2"
        '
        'tsb_edit
        '
        Me.tsb_edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_edit.Image = Global.madeinmekong.My.Resources.Resources.baseline_edit_black_18dp
        Me.tsb_edit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_edit.Name = "tsb_edit"
        Me.tsb_edit.Size = New System.Drawing.Size(26, 29)
        Me.tsb_edit.Text = "ToolStripButton1"
        '
        'tsb_close
        '
        Me.tsb_close.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.tsb_close.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_close.Image = CType(resources.GetObject("tsb_close.Image"), System.Drawing.Image)
        Me.tsb_close.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_close.Name = "tsb_close"
        Me.tsb_close.RightToLeftAutoMirrorImage = True
        Me.tsb_close.Size = New System.Drawing.Size(26, 29)
        Me.tsb_close.Text = "Close"
        '
        'tsb_add
        '
        Me.tsb_add.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_add.Image = CType(resources.GetObject("tsb_add.Image"), System.Drawing.Image)
        Me.tsb_add.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_add.Name = "tsb_add"
        Me.tsb_add.Size = New System.Drawing.Size(26, 29)
        Me.tsb_add.Text = "add"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.gb_add)
        Me.GroupBox2.Controls.Add(Me.dg_data)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(800, 540)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "ຂໍ້ມູນໂຕະ"
        '
        'gb_add
        '
        Me.gb_add.Controls.Add(Me.chk_enabled)
        Me.gb_add.Controls.Add(Me.btn_closeadd)
        Me.gb_add.Controls.Add(Me.btn_store)
        Me.gb_add.Controls.Add(Me.txt_tablesname)
        Me.gb_add.Controls.Add(Me.Label1)
        Me.gb_add.Location = New System.Drawing.Point(517, 404)
        Me.gb_add.Name = "gb_add"
        Me.gb_add.Size = New System.Drawing.Size(277, 130)
        Me.gb_add.TabIndex = 1
        Me.gb_add.TabStop = False
        Me.gb_add.Text = "ເພິ່ມໂຕະ"
        Me.gb_add.Visible = False
        '
        'chk_enabled
        '
        Me.chk_enabled.AutoSize = True
        Me.chk_enabled.Location = New System.Drawing.Point(158, 26)
        Me.chk_enabled.Name = "chk_enabled"
        Me.chk_enabled.Size = New System.Drawing.Size(118, 27)
        Me.chk_enabled.TabIndex = 4
        Me.chk_enabled.Text = "ເປິດການໃຊ້ງານ"
        Me.chk_enabled.UseVisualStyleBackColor = True
        '
        'btn_closeadd
        '
        Me.btn_closeadd.Location = New System.Drawing.Point(167, 90)
        Me.btn_closeadd.Name = "btn_closeadd"
        Me.btn_closeadd.Size = New System.Drawing.Size(104, 31)
        Me.btn_closeadd.TabIndex = 3
        Me.btn_closeadd.Text = "ປິດ"
        Me.btn_closeadd.UseVisualStyleBackColor = True
        '
        'btn_store
        '
        Me.btn_store.Location = New System.Drawing.Point(10, 90)
        Me.btn_store.Name = "btn_store"
        Me.btn_store.Size = New System.Drawing.Size(151, 31)
        Me.btn_store.TabIndex = 2
        Me.btn_store.Text = "ບັນທຶກ"
        Me.btn_store.UseVisualStyleBackColor = True
        '
        'txt_tablesname
        '
        Me.txt_tablesname.Location = New System.Drawing.Point(10, 53)
        Me.txt_tablesname.Name = "txt_tablesname"
        Me.txt_tablesname.Size = New System.Drawing.Size(261, 31)
        Me.txt_tablesname.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ໂຕະ"
        '
        'dg_data
        '
        Me.dg_data.AllowUserToAddRows = False
        Me.dg_data.AllowUserToDeleteRows = False
        Me.dg_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_data.ContextMenuStrip = Me.ctm_dg
        Me.dg_data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_data.Location = New System.Drawing.Point(3, 27)
        Me.dg_data.Name = "dg_data"
        Me.dg_data.ReadOnly = True
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dg_data.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dg_data.RowTemplate.Height = 30
        Me.dg_data.Size = New System.Drawing.Size(794, 510)
        Me.dg_data.TabIndex = 0
        '
        'ctm_dg
        '
        Me.ctm_dg.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_edit, Me.tsm_del})
        Me.ctm_dg.Name = "ContextMenuStrip1"
        Me.ctm_dg.Size = New System.Drawing.Size(105, 60)
        '
        'tsm_edit
        '
        Me.tsm_edit.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsm_edit.Image = Global.madeinmekong.My.Resources.Resources.baseline_edit_black_18dp
        Me.tsm_edit.Name = "tsm_edit"
        Me.tsm_edit.Size = New System.Drawing.Size(104, 28)
        Me.tsm_edit.Text = "Edit"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(62, 23)
        Me.ToolStripStatusLabel1.Text = "ສະຖານະ:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.tsl_id, Me.tsl_status})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 572)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(800, 28)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsl_id
        '
        Me.tsl_id.Name = "tsl_id"
        Me.tsl_id.Size = New System.Drawing.Size(0, 23)
        '
        'tsl_status
        '
        Me.tsl_status.ForeColor = System.Drawing.Color.Red
        Me.tsl_status.Name = "tsl_status"
        Me.tsl_status.Size = New System.Drawing.Size(0, 23)
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(22, 22)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.ToolStripSeparator1, Me.tsb_add, Me.tsb_close, Me.tsb_edit, Me.tsb_del, Me.ToolStripSeparator2, Me.ToolStripLabel2, Me.txt_search})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(800, 32)
        Me.ToolStrip1.TabIndex = 10
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(33, 29)
        Me.ToolStripLabel1.Text = "ໂຕະ"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 32)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 32)
        '
        'txt_search
        '
        Me.txt_search.AutoSize = False
        Me.txt_search.Name = "txt_search"
        Me.txt_search.Size = New System.Drawing.Size(250, 32)
        '
        'tables
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 23.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 600)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "tables"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "tables"
        Me.GroupBox2.ResumeLayout(False)
        Me.gb_add.ResumeLayout(False)
        Me.gb_add.PerformLayout()
        CType(Me.dg_data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctm_dg.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tsm_del As ToolStripMenuItem
    Friend WithEvents ToolStripLabel2 As ToolStripLabel
    Friend WithEvents tsb_del As ToolStripButton
    Friend WithEvents tsb_edit As ToolStripButton
    Friend WithEvents tsb_close As ToolStripButton
    Friend WithEvents tsb_add As ToolStripButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents gb_add As GroupBox
    Friend WithEvents btn_closeadd As Button
    Friend WithEvents btn_store As Button
    Friend WithEvents txt_tablesname As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents dg_data As DataGridView
    Friend WithEvents ctm_dg As ContextMenuStrip
    Friend WithEvents tsm_edit As ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsl_id As ToolStripStatusLabel
    Friend WithEvents tsl_status As ToolStripStatusLabel
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents txt_search As ToolStripTextBox
    Friend WithEvents chk_enabled As CheckBox
End Class
