﻿Public Class foods
    Private clCon, clC, clC1 As New ClConnections
    Private dt, dtt, dtu As New DataTable
    Dim dataId As String
    Dim edit As Boolean
    Private Sub Foods_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dt.Clear()
        dt = clCon.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
        [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
        [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[name] As type 
        FROM [dbo].[tb_foods] 
        INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
        INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid]")
        Me.dg_data.DataSource = dt
        Me.dg_data.Columns.Item("Id").HeaderText = "ລະຫັດ"
        Me.dg_data.Columns.Item("name").HeaderText = "ຊື່"
        Me.dg_data.Columns.Item("name").Width = 200
        Me.dg_data.Columns.Item("size").HeaderText = "ຂະໜາດ"
        Me.dg_data.Columns.Item("price").HeaderText = "ລາຄາ"
        Me.dg_data.Columns.Item("unit").HeaderText = "ຫົວໜ່ວຍ"
        Me.dg_data.Columns.Item("type").HeaderText = "ປະເພດ"
        Me.dg_data.Columns.Item("unitid").Visible = False
        Me.dg_data.Columns.Item("typeid").Visible = False
        dtt = clC.loadData("SELECT * FROM [dbo].[tb_types] WHERE [stocks]=0")
        Me.cbx_types.DataSource = dtt
        Me.cbx_types.DisplayMember = "name"
        Me.cbx_types.ValueMember = "Id"
    End Sub

    Private Sub Tsb_add_Click(sender As Object, e As EventArgs) Handles tsb_add.Click
        edit = False
        gb_add.Text = "ເພິ່ມອາຫານ"
        Me.txt_name.Clear()
        Me.txt_size.Clear()
        Me.txt_price.Clear()
        Me.cbx_types.SelectedIndex = 0
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_edit_Click(sender As Object, e As EventArgs) Handles tsb_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂອາຫານ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsm_edit_Click(sender As Object, e As EventArgs) Handles tsm_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂອາຫານ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_del_Click(sender As Object, e As EventArgs) Handles tsb_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບອາຫານ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_foods] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
        [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
        [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[Id] As type 
        FROM [dbo].[tb_foods] 
        INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
        INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Tsm_del_Click(sender As Object, e As EventArgs) Handles tsm_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບອາຫານ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_foods] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
        [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
        [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[Id] As type 
        FROM [dbo].[tb_foods] 
        INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
        INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Cbx_types_Leave(sender As Object, e As EventArgs) Handles cbx_types.Leave
        dtu.Clear()
        Me.cbx_unit.DataSource = Nothing
        dtu = clC1.loadData("SELECT * FROM [dbo].[tb_unit] WHERE [typeid]=" + Me.cbx_types.SelectedValue.ToString)
        Me.cbx_unit.DataSource = dtu
        Me.cbx_unit.DisplayMember = "name"
        Me.cbx_unit.ValueMember = "Id"
        Me.cbx_unit.Refresh()
    End Sub

    Private Sub Tsb_close_Click(sender As Object, e As EventArgs) Handles tsb_close.Click
        Me.Close()
    End Sub

    Private Sub Txt_search_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            dt.Clear()
            dt = clCon.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
            [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
            [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[Id] As type 
            FROM [dbo].[tb_foods] 
            INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid] 
            WHERE [dbo].[tb_foods].[name] LIKE N'%" + Me.txt_search.Text + "%' OR [dbo].[tb_foods].[Id] LIKE '%" + Me.txt_search.Text + "%'")
            Me.dg_data.Refresh()
        End If
    End Sub

    Private Sub Dg_data_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_data.CellEnter
        Dim id = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        dataId = id
        Me.tsl_id.Text = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        If edit = True Then
            Me.txt_name.Text = Me.dg_data.Rows(e.RowIndex).Cells("name").Value
            Me.txt_size.Text = Me.dg_data.Rows(e.RowIndex).Cells("size").Value
            Me.txt_price.Text = Me.dg_data.Rows(e.RowIndex).Cells("price").Value
            Me.cbx_types.SelectedValue = Me.dg_data.Rows(e.RowIndex).Cells("typeid").Value
            dtu.Clear()
            Me.cbx_unit.DataSource = Nothing
            dtu = clC1.loadData("SELECT * FROM [dbo].[tb_unit] WHERE [typeid]=" + Me.dg_data.Rows(e.RowIndex).Cells("typeid").Value.ToString)
            Me.cbx_unit.DataSource = dtu
            Me.cbx_unit.DisplayMember = "name"
            Me.cbx_unit.ValueMember = "Id"
            Me.cbx_unit.Refresh()
            Me.cbx_unit.SelectedValue = Me.dg_data.Rows(e.RowIndex).Cells("unitid").Value
        End If
        Me.tsl_status.Text = ""
    End Sub

    Private Sub Btn_store_Click(sender As Object, e As EventArgs) Handles btn_store.Click
        If edit = True Then
            Dim rs As Boolean = clCon.storeData("UPDATE [dbo].[tb_foods] 
            SET [name]=N'" + txt_name.Text + "',
            [size]='" + txt_size.Text + "',
            [price]='" + txt_price.Text + "',
            [unitid]='" + cbx_unit.SelectedValue.ToString + "',
            [typeid]='" + cbx_types.SelectedValue.ToString + "' 
            WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
            [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
            [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[Id] As type 
            FROM [dbo].[tb_foods] 
            INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ປັບປຸງຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການປັບປຸງຂໍ້ມູນ"
            End If
        Else
            Dim rs As Boolean = clCon.storeData("INSERT INTO [dbo].[tb_foods] ([name], [size], [price], [unitid], [typeid]) 
            VALUES (N'" + txt_name.Text + "', N'" + txt_size.Text + "', '" + txt_price.Text +
            "', '" + cbx_unit.SelectedValue.ToString + "', '" + cbx_types.SelectedValue.ToString + "')")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
            [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
            [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[Id] As type 
            FROM [dbo].[tb_foods] 
            INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ເພິ່ມຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການເພິ່ມຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Btn_closeadd_Click(sender As Object, e As EventArgs) Handles btn_closeadd.Click
        gb_add.Visible = False
    End Sub

End Class