﻿Public Class tables
    Private clCon As New ClConnections
    Private dt As New DataTable
    Dim dataId As String
    Dim edit As Boolean
    Private Sub Tables_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dt = clCon.loadData("SELECT * FROM [dbo].[tb_tables]")
        Me.dg_data.DataSource = dt
        Me.dg_data.Columns.Item("Id").HeaderText = "ລະຫັດ"
        Me.dg_data.Columns.Item("name").HeaderText = "ໂຕະ"
        Me.dg_data.Columns.Item("name").Width = 200
        Me.dg_data.Columns.Item("status").HeaderText = "ສະຖານະ"
        Me.dg_data.Columns.Item("enabled").HeaderText = "ເປີດການໃຊ້ງານ"
    End Sub

    Private Sub Tsb_add_Click(sender As Object, e As EventArgs) Handles tsb_add.Click
        edit = False
        gb_add.Text = "ເພິ່ມໂຕະ"
        txt_tablesname.Clear()
        chk_enabled.Checked = False
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_edit_Click(sender As Object, e As EventArgs) Handles tsb_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂໂຕະ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsm_edit_Click(sender As Object, e As EventArgs) Handles tsm_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂໂຕະ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_del_Click(sender As Object, e As EventArgs) Handles tsb_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບໂຕະ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_tables] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_tables]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Tsm_del_Click(sender As Object, e As EventArgs) Handles tsm_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບໂຕະ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_tables] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_tables]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Btn_store_Click(sender As Object, e As EventArgs) Handles btn_store.Click
        Dim ck As Integer = 0
        If chk_enabled.Checked = True Then
            ck = 1
        End If
        If edit = True Then
            Dim rs As Boolean = clCon.storeData("UPDATE [dbo].[tb_tables] SET [name]=N'" + txt_tablesname.Text + "',[enabled]='" + ck.ToString + "' WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_tables]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ປັບປຸງຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການປັບປຸງຂໍ້ມູນ"
            End If
        Else
            Dim rs As Boolean = clCon.storeData("INSERT INTO [dbo].[tb_tables] ([name], [enabled]) VALUES (N'" + txt_tablesname.Text + "', '" + ck.ToString + "')")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_tables]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ເພິ່ມຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການເພິ່ມຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Btn_closeadd_Click(sender As Object, e As EventArgs) Handles btn_closeadd.Click
        gb_add.Visible = False
    End Sub

    Private Sub Dg_data_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_data.CellEnter
        Dim id = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        dataId = id
        Me.tsl_id.Text = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        If edit = True Then
            Me.txt_tablesname.Text = Me.dg_data.Rows(e.RowIndex).Cells(1).Value
            If Me.dg_data.Rows(e.RowIndex).Cells(3).Value = True Then
                Me.chk_enabled.Checked = True
            Else
                Me.chk_enabled.Checked = False
            End If
        End If
        Me.tsl_status.Text = ""
    End Sub

    Private Sub Tsb_close_Click(sender As Object, e As EventArgs) Handles tsb_close.Click
        Me.Close()
    End Sub

    Private Sub Txt_search_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            dt.Clear()
            dt = clCon.loadData("SELECT * FROM [dbo].[tb_tables] WHERE [name] LIKE N'%" + Me.txt_search.Text + "%' OR [Id] LIKE '%" + Me.txt_search.Text + "%'")
            Me.dg_data.Refresh()
        End If
    End Sub
End Class