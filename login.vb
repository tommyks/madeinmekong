﻿Public Class login
    'Create a Connection object
    Private clCon As New ClConnections
    'Create variable
    Public id As Integer
    Private ctn As Boolean
    'Create Login Sub to use in 2 Event Login Click and TextBox KeyDown(Keyboard Enter To Login)
    Private Sub loginevent()
        'Enabled Timer1
        Me.Timer1.Enabled = True
        'User Function Login in Class ClConnections (Connection object)
        Dim rs = clCon.login(Me.txt_username.Text.ToString, Me.txt_password.Text.ToString)
        'Check failed login
        If rs <> False Then
            'True Login
            ctn = True
            'Set ID
            id = rs
        Else
            ctn = False
        End If
    End Sub
    Private Sub Btn_login_Click(sender As Object, e As EventArgs) Handles btn_login.Click
        'Use loginevent Sub
        Me.loginevent()
    End Sub
    'Timer to load ProgressBar
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'If ProgressBar < 100 Block ProgressBar error
        If Me.ToolStripProgressBar1.Value < 100 Then
            Me.ToolStripProgressBar1.Value += 2
        End If
        'If ProgressBar stop to continue 
        If Me.ToolStripProgressBar1.Value >= 98 Then
            'If Login true
            If ctn = True Then
                'Show Main Form
                main.Show()
                'Clear Password
                Me.txt_password.Clear()
                'Reset ProgressBar
                Me.ToolStripProgressBar1.Value = 0
                'Stop Timer
                Me.Timer1.Enabled = False
                'Hide Login Form
                Me.Hide()
            Else 'Else Enter User or Password is not matched
                lab_wrongpassword.Text = "User or Password is not matched"
                Me.txt_password.Clear()
                Me.ToolStripProgressBar1.Value = 0
                Me.Timer1.Enabled = False
            End If
        End If
    End Sub

    Private Sub Btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        'Exit Appliction
        Application.Exit()
    End Sub

    Private Sub Txt_password_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txt_password.KeyPress
        'Reset Notification in Txt_password on used
        lab_wrongpassword.Text = ""
    End Sub

    Private Sub Txt_password_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_password.KeyDown
        'If keyboard Key Enter
        If e.KeyCode = Keys.Enter Then
            'Use loginevent Sub
            Me.loginevent()
        End If
    End Sub
End Class
