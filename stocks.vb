﻿Public Class stocks
    Private clCon, clC, clC1 As New ClConnections
    Private dt, dtt, dtu As New DataTable
    Dim dataId As String
    Dim edit As Boolean

    Private Sub Stocks_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dt.Clear()
        dt = clCon.loadData("SELECT * FROM dbo.v_stocks")
        Me.dg_data.DataSource = dt
        Me.dg_data.Columns.Item("Id").HeaderText = "ລະຫັດ"
        Me.dg_data.Columns.Item("stockno").HeaderText = "ໝາຍເລກ"
        Me.dg_data.Columns.Item("stockno").Width = 100
        Me.dg_data.Columns.Item("stockname").HeaderText = "ຊື່"
        Me.dg_data.Columns.Item("stockname").Width = 100
        Me.dg_data.Columns.Item("qty").HeaderText = "ເບີໂທລະສັບ"
        Me.dg_data.Columns.Item("stock").HeaderText = "ເຂົ້າ/ອອກ"
        Me.dg_data.Columns.Item("unitid").Visible = False
        Me.dg_data.Columns.Item("typeid").Visible = False
        Me.dg_data.Columns.Item("unit").HeaderText = "ຫົວໜ່ວຍ"
        Me.dg_data.Columns.Item("type").HeaderText = "ປະເພດ"
        Me.dg_data.Columns.Item("enabled").HeaderText = "ເປີດປີດນຳໃຊ້"
        Me.dg_data.Columns.Item("status").HeaderText = "ສະຖານະ"
        Me.dg_data.Columns.Item("datetime").HeaderText = "ວັນເດືອນປີ"
        Me.cbx_stock.DisplayMember = "Text"
        Me.cbx_stock.ValueMember = "Value"
        Me.cbx_stock.Items.Add(New With {.Text = "ເຂົ້າ", .Value = 0})
        Me.cbx_stock.Items.Add(New With {.Text = "ອອກ", .Value = 1})
        dtt = clC.loadData("SELECT * FROM [dbo].[tb_types] WHERE [stocks]=1")
        Me.cbx_type.DataSource = dtt
        Me.cbx_type.DisplayMember = "name"
        Me.cbx_type.ValueMember = "Id"
    End Sub

    Private Sub Tsb_add_Click(sender As Object, e As EventArgs) Handles tsb_add.Click
        edit = False
        gb_add.Text = "ເພິ່ມພະນັກງານ"
        Me.txt_no.Clear()
        Me.txt_name.Clear()
        Me.txt_qty.Clear()
        Me.cbx_stock.SelectedIndex = 0
        Me.cbx_type.SelectedIndex = 0
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_edit_Click(sender As Object, e As EventArgs) Handles tsb_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂນຳເຂົ້າ-ອອກສ່າງສິນຄ້າ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_del_Click(sender As Object, e As EventArgs) Handles tsb_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບຂໍ້ມູນພະນັກງານ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_stocks] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[v_stocks]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Tsm_edit_Click(sender As Object, e As EventArgs) Handles tsm_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂນຳເຂົ້າ-ອອກສ່າງສິນຄ້າ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsm_del_Click(sender As Object, e As EventArgs) Handles tsm_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບຂໍ້ມູນພະນັກງານ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_stocks] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[v_stocks]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Dg_data_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_data.CellEnter
        Dim id = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        dataId = id
        Me.tsl_id.Text = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        If edit = True Then
            Me.txt_no.Text = Me.dg_data.Rows(e.RowIndex).Cells("stockno").Value
            Me.txt_name.Text = Me.dg_data.Rows(e.RowIndex).Cells("stockname").Value
            Me.txt_qty.Text = Me.dg_data.Rows(e.RowIndex).Cells("qty").Value
            Me.cbx_stock.SelectedIndex = Me.dg_data.Rows(e.RowIndex).Cells("stock").Value
            Me.cbx_type.SelectedValue = Me.dg_data.Rows(e.RowIndex).Cells("typeid").Value
            Me.cbx_unit.SelectedValue = Me.dg_data.Rows(e.RowIndex).Cells("unitid").Value
        End If
        Me.tsl_status.Text = ""
    End Sub

    Private Sub Btn_store_Click(sender As Object, e As EventArgs) Handles btn_store.Click
        If edit = True Then
            Dim rs As Boolean = clCon.storeData("UPDATE [dbo].[tb_stocks] 
            SET [stockno]=N'" + txt_no.Text + "',
            [stockname]=N'" + txt_name.Text + "',
            [qty]='" + txt_qty.Text + "',
            [stock]='" + cbx_stock.SelectedItem.Value.ToString + "',
            [unitid]='" + cbx_unit.SelectedValue.ToString + "', 
            [typeid]='" + cbx_type.SelectedValue.ToString + "', 
            [datetime]=N'" + Now.ToString("yyyy-MM-dd HH:MM:ss") + "' 
            WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[v_stocks]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ປັບປຸງຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການປັບປຸງຂໍ້ມູນ"
            End If
        Else
            Dim rs As Boolean = clCon.storeData("INSERT INTO [dbo].[tb_stocks] ([stockno], [stockname], [qty], [stock], [unitid], [typeid], [datetime]) 
            VALUES (N'" + txt_no.Text + "', N'" + txt_name.Text + "', '" + txt_qty.Text +
            "', '" + cbx_stock.SelectedItem.Value.ToString + "', '" + cbx_unit.SelectedValue.ToString + "', '" + cbx_type.SelectedValue.ToString + "', N'" + Now.ToString("yyyy-MM-dd HH:MM:ss") + "')")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[v_stocks]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ເພິ່ມຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການເພິ່ມຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Txt_search_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            dt.Clear()
            dt = clCon.loadData("SELECT * FROM [dbo].[v_stocks] 
            WHERE [no] LIKE N'%" + Me.txt_search.Text + "%' 
            OR [name] LIKE N'%" + Me.txt_search.Text + "%'
            OR [qty] LIKE N'%" + Me.txt_search.Text + "%'
            OR [Id] LIKE '%" + Me.txt_search.Text + "%'")
            Me.dg_data.Refresh()
        End If
    End Sub

    Private Sub Btn_closeadd_Click(sender As Object, e As EventArgs) Handles btn_closeadd.Click
        gb_add.Visible = False
    End Sub

    Private Sub Cbx_type_Leave(sender As Object, e As EventArgs) Handles cbx_type.Leave
        dtu.Clear()
        Me.cbx_unit.DataSource = Nothing
        dtu = clC1.loadData("SELECT * FROM [dbo].[tb_unit] WHERE [typeid]=" + Me.cbx_type.SelectedValue.ToString)
        Me.cbx_unit.DataSource = dtu
        Me.cbx_unit.DisplayMember = "name"
        Me.cbx_unit.ValueMember = "Id"
        Me.cbx_unit.Refresh()
    End Sub
End Class