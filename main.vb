﻿Imports System.Drawing.Printing

Public Class main
    Private clCon, conBill, conTables, conFoods, conOrders, conTotal, conSum, conTablestate, conTableschange, conTransac, conInvoice As New ClConnections
    Private userid As Integer
    Private dt, dtbill, dt_tables, dt_foods, dt_orders, dt_total, dt_sum, dt_tablestate, dt_tablechange, dt_transac, dt_invoice As New DataTable
    Private username, billnumber, orderid As String
    Private printFont As Font
    Private totalLineCount As Integer

    Private Sub Chk_tables_CheckedChanged(sender As Object, e As EventArgs) Handles chk_tables.CheckedChanged
        checktabelstatus()
    End Sub

    Private Sub Btn_changetables_Click(sender As Object, e As EventArgs) Handles btn_changetables.Click
        gb_changetables.Visible = True
    End Sub

    Private Sub Btn_checkout_Click(sender As Object, e As EventArgs) Handles btn_checkout.Click
        Try
            Try
                Dim result As Integer = MessageBox.Show("ຕ້ອງການໄລ່ເງີນໂຕະ: " + cbx_tables.Text.ToString + " ນີ້ບໍ່?", "ຄິດໄລ່ເງິນ", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then
                    Dim ts As Boolean = conTransac.storeData("INSERT INTO [dbo].[tb_transactions] ([no], [amount], [datetime]) 
                        VALUES ('" + txt_billno.Text.ToString + "', '" + lab_total.Text.ToString + "', N'" + Now.ToString("yyyy-MM-dd HH:MM:ss") + "')")
                    If ts = True Then
                        Dim rs As Boolean = conOrders.storeData("UPDATE [dbo].[tb_orders] SET [dbo].[tb_orders].[status]=0, [dbo].[tb_orders].[transactionid]='" + dt.Rows(0).ItemArray(0).ToString + "'
                            WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0")
                        If rs = True Then
                            conTablestate.storeData("UPDATE [dbo].[tb_tables] SET [dbo].[tb_tables].[status] = 1  
                                WHERE [dbo].[tb_tables].[Id] = '" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_tables].[status] <> 1")
                            checktabelstatus()
                            dtbill.Clear()
                            dtbill = conBill.loadData("Select IDENT_CURRENT('tb_transactions') AS Current_Identity;")
                            billnumber = 1000 + CInt(dt.Rows(0).ItemArray(0).ToString)
                            txt_billno.Text = "MM" + billnumber.ToString
                            dt_orders.Clear()
                            dt_orders = conOrders.loadData("SELECT [dbo].[tb_orders].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
                                [dbo].[tb_foods].[price] As price,[dbo].[tb_unit].[name] As unit,[dbo].[tb_orders].[qty] As qty,[dbo].[tb_orders].[datetime] As datetime
                                FROM [dbo].[tb_orders] 
                                INNER JOIN [dbo].[tb_foods] ON [dbo].[tb_foods].[Id] = [dbo].[tb_orders].[foodid] 
                                INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
                                WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0")
                            Me.dg_ordersfood.Refresh()
                            dt_total.Clear()
                            dt_total = conTotal.loadData("SELECT dbo.tb_foods.Id,dbo.tb_foods.name, dbo.tb_foods.size, dbo.tb_unit.name AS unit, dbo.tb_foods.price, SUM(dbo.tb_orders.qty) AS qty,
                                             dbo.tb_foods.price * SUM(dbo.tb_orders.qty) AS total,MAX(DISTINCT dbo.tb_orders.datetime) AS datetime 
                                    FROM dbo.tb_orders 
                                    INNER JOIN dbo.tb_foods ON dbo.tb_orders.foodid = dbo.tb_foods.Id INNER JOIN
                                               dbo.tb_unit ON dbo.tb_foods.unitid = dbo.tb_unit.Id
                                    WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "'AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0 
                                    GROUP BY dbo.tb_foods.Id, dbo.tb_foods.price, dbo.tb_foods.size, dbo.tb_foods.name, dbo.tb_unit.name")
                            Me.dg_total.Refresh()
                            dt_sum.Clear()
                            dt_sum = conSum.loadData("SELECT SUM(dbo.v_total.total) AS Total, MAX(DISTINCT dbo.v_total.datetime) AS datetime FROM dbo.v_total WHERE [tableid]='" + cbx_tables.SelectedValue.ToString + "'")
                            Me.lab_total.Text = dt_sum.Rows(0).ItemArray(0).ToString
                            Me.lab_datetime.Text = dt_sum.Rows(0).ItemArray(1).ToString
                            totalLineCount = 0
                            printFont = New Font("Phetsarath OT", 12)
                            Dim printDoc As New PrintDocument()
                            AddHandler printDoc.PrintPage, AddressOf Me.printDoc_PrintPage
                            printDoc.Print()
                        End If
                    End If
                End If
            Finally
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub printDoc_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim curLineCount As Integer = 0
        Dim leftMargin As Single = e.MarginBounds.Left
        Dim topMargin As Single = e.MarginBounds.Top
        Dim line As String = Nothing

        ' Calculate the number of lines per page.
        linesPerPage = e.MarginBounds.Height / printFont.GetHeight(e.Graphics)

        ' Print each line of the file. 
        While curLineCount < linesPerPage
            If (RichTextBox1.Lines.Length - 1) < totalLineCount Then
                line = Nothing
                Exit While
            End If
            line = RichTextBox1.Lines(totalLineCount)
            yPos = topMargin + curLineCount * printFont.GetHeight(e.Graphics)
            e.Graphics.DrawString(line, printFont, Brushes.Black, leftMargin, yPos, New StringFormat())
            curLineCount += 1
            totalLineCount += 1
        End While

        ' If more lines exist, print another page. 
        If (line IsNot Nothing) Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
        End If
    End Sub

    Private Sub Txt_search_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            If chk_tables.CheckState = CheckState.Checked Then
                dt_tablestate.Clear()
                dt_tablestate = conTablestate.loadData("SELECT dbo.tb_tables.Id, dbo.tb_tables.name
                                                FROM dbo.tb_tables 
                                                WHERE (dbo.tb_tables.status = 1 AND dbo.tb_tables.Id LIKE N'%" + txt_search.Text.ToString + "%') OR
                                                      (dbo.tb_tables.status = 1 AND dbo.tb_tables.name LIKE N'%" + txt_search.Text.ToString + "%')")
                Me.dg_tablestate.Refresh()
            Else
                dt_tablestate.Clear()
                dt_tablestate = conTablestate.loadData("SELECT dbo.tb_tables.Id, dbo.tb_tables.name
                                                FROM dbo.tb_tables 
                                                WHERE (dbo.tb_tables.status = 0 AND dbo.tb_tables.Id LIKE N'%" + txt_search.Text.ToString + "%') OR
                                                      (dbo.tb_tables.status = 0 AND dbo.tb_tables.name LIKE N'%" + txt_search.Text.ToString + "%')")
                Me.dg_tablestate.Refresh()
            End If
        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim curLineCount As Integer = 0
        Dim leftMargin As Single = e.MarginBounds.Left
        Dim topMargin As Single = e.MarginBounds.Top
        Dim line As String = Nothing

        ' Calculate the number of lines per page.
        linesPerPage = e.MarginBounds.Height / printFont.GetHeight(e.Graphics)

        ' Print each line of the file. 
        While curLineCount < linesPerPage
            If (RichTextBox1.Lines.Length - 1) < totalLineCount Then
                line = Nothing
                Exit While
            End If
            line = RichTextBox1.Lines(totalLineCount)
            yPos = topMargin + curLineCount * printFont.GetHeight(e.Graphics)
            e.Graphics.DrawString(line, printFont, Brushes.Black, leftMargin, yPos, New StringFormat())
            curLineCount += 1
            totalLineCount += 1
        End While

        ' If more lines exist, print another page. 
        If (line IsNot Nothing) Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
        End If
    End Sub

    Private Sub Btn_changetb_Click(sender As Object, e As EventArgs) Handles btn_changetb.Click
        Dim rs As Boolean = conOrders.storeData("UPDATE [dbo].[tb_orders] SET [dbo].[tb_orders].[tableid]='" + cbx_changetables.SelectedValue.ToString + "' 
        WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0")
        If rs = True Then
            conTablestate.storeData("UPDATE [dbo].[tb_tables] SET [dbo].[tb_tables].[status] = 0  
                WHERE [dbo].[tb_tables].[Id] = '" + cbx_changetables.SelectedValue.ToString + "' AND [dbo].[tb_tables].[status] <> 0")
            conTablestate.storeData("UPDATE [dbo].[tb_tables] SET [dbo].[tb_tables].[status] = 1  
                WHERE [dbo].[tb_tables].[Id] = '" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_tables].[status] <> 1")
            cbx_tables.SelectedValue = cbx_changetables.SelectedValue
            checktabelstatus()
            dt_orders.Clear()
            dt_orders = conOrders.loadData("SELECT [dbo].[tb_orders].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
                [dbo].[tb_foods].[price] As price,[dbo].[tb_unit].[name] As unit,[dbo].[tb_orders].[qty] As qty,[dbo].[tb_orders].[datetime] As datetime
                FROM [dbo].[tb_orders] 
                INNER JOIN [dbo].[tb_foods] ON [dbo].[tb_foods].[Id] = [dbo].[tb_orders].[foodid] 
                INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
                WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0")
            Me.dg_ordersfood.Refresh()
            dt_total.Clear()
            dt_total = conTotal.loadData("SELECT dbo.tb_foods.Id,dbo.tb_foods.name, dbo.tb_foods.size, dbo.tb_unit.name AS unit, dbo.tb_foods.price, SUM(dbo.tb_orders.qty) AS qty,
                                             dbo.tb_foods.price * SUM(dbo.tb_orders.qty) AS total,MAX(DISTINCT dbo.tb_orders.datetime) AS datetime 
                                    FROM dbo.tb_orders 
                                    INNER JOIN dbo.tb_foods ON dbo.tb_orders.foodid = dbo.tb_foods.Id INNER JOIN
                                               dbo.tb_unit ON dbo.tb_foods.unitid = dbo.tb_unit.Id
                                    WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "'AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0 
                                    GROUP BY dbo.tb_foods.Id, dbo.tb_foods.price, dbo.tb_foods.size, dbo.tb_foods.name, dbo.tb_unit.name")
            Me.dg_total.Refresh()
            dt_sum.Clear()
            dt_sum = conSum.loadData("SELECT SUM(dbo.v_total.total) AS Total, MAX(DISTINCT dbo.v_total.datetime) AS datetime FROM dbo.v_total WHERE [tableid]='" + cbx_tables.SelectedValue.ToString + "'")
            Me.lab_total.Text = dt_sum.Rows(0).ItemArray(0).ToString
            Me.lab_datetime.Text = dt_sum.Rows(0).ItemArray(1).ToString
        End If
    End Sub

    Private Sub Btn_closechangetable_Click(sender As Object, e As EventArgs) Handles btn_closechangetable.Click
        gb_changetables.Visible = False
        cbx_changetables.SelectedIndex = 0
    End Sub

    Private Sub checktabelstatus()
        If chk_tables.CheckState = CheckState.Checked Then
            dt_tablestate.Clear()
            dt_tablestate = conTablestate.loadData("SELECT dbo.tb_tables.Id, dbo.tb_tables.name
                                                FROM dbo.tb_tables 
                                                WHERE (dbo.tb_tables.status = 1)")
            Me.dg_tablestate.Refresh()
        Else
            dt_tablestate.Clear()
            dt_tablestate = conTablestate.loadData("SELECT dbo.tb_tables.Id, dbo.tb_tables.name
                                                FROM dbo.tb_tables 
                                                WHERE (dbo.tb_tables.status = 0)")
            Me.dg_tablestate.Refresh()
        End If
    End Sub
    Private Sub Tsb_exit_Click(sender As Object, e As EventArgs) Handles tsb_exit.Click
        Application.Exit()
    End Sub

    Private Sub Btn_order_Click(sender As Object, e As EventArgs) Handles btn_order.Click
        If txt_qty.Text > 0 Then
            Dim rs As Boolean = conOrders.storeData("INSERT INTO [dbo].[tb_orders] 
                ([qty], [foodid], [tableid], [datetime]) 
                VALUES ('" + txt_qty.Text + "', '" + orderid.ToString + "', '" + cbx_tables.SelectedValue.ToString + "', N'" + Now.ToString("yyyy-MM-dd HH:MM:ss") + "')")
            If rs = True Then
                conTablestate.storeData("UPDATE [dbo].[tb_tables] SET [dbo].[tb_tables].[status] = 0  
                WHERE [dbo].[tb_tables].[Id] = '" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_tables].[status] <> 0")
                checktabelstatus()
                dt_orders.Clear()
                dt_orders = conOrders.loadData("SELECT [dbo].[tb_orders].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
                [dbo].[tb_foods].[price] As price,[dbo].[tb_unit].[name] As unit,[dbo].[tb_orders].[qty] As qty,[dbo].[tb_orders].[datetime] As datetime
                FROM [dbo].[tb_orders] 
                INNER JOIN [dbo].[tb_foods] ON [dbo].[tb_foods].[Id] = [dbo].[tb_orders].[foodid] 
                INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
                WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0")
                Me.dg_ordersfood.Refresh()
                dt_total.Clear()
                dt_total = conTotal.loadData("SELECT dbo.tb_foods.Id,dbo.tb_foods.name, dbo.tb_foods.size, dbo.tb_unit.name AS unit, dbo.tb_foods.price, SUM(dbo.tb_orders.qty) AS qty,
                                             dbo.tb_foods.price * SUM(dbo.tb_orders.qty) AS total,MAX(DISTINCT dbo.tb_orders.datetime) AS datetime 
                                    FROM dbo.tb_orders 
                                    INNER JOIN dbo.tb_foods ON dbo.tb_orders.foodid = dbo.tb_foods.Id INNER JOIN
                                               dbo.tb_unit ON dbo.tb_foods.unitid = dbo.tb_unit.Id
                                    WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0
                                    GROUP BY dbo.tb_foods.Id, dbo.tb_foods.price, dbo.tb_foods.size, dbo.tb_foods.name, dbo.tb_unit.name")
                Me.dg_total.Refresh()
                dt_sum.Clear()
                dt_sum = conSum.loadData("SELECT SUM(dbo.v_total.total) AS Total, MAX(DISTINCT dbo.v_total.datetime) AS datetime FROM dbo.v_total WHERE [tableid]='" + cbx_tables.SelectedValue.ToString + "'")
                Me.lab_total.Text = dt_sum.Rows(0).ItemArray(0).ToString
                Me.lab_datetime.Text = dt_sum.Rows(0).ItemArray(1).ToString
            End If
        End If

    End Sub

    Private Sub Dg_orderfood_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_food.CellEnter
        orderid = Me.dg_food.Rows(e.RowIndex).Cells("Id").Value.ToString
        lb_foodname.Text = Me.dg_food.Rows(e.RowIndex).Cells("name").Value.ToString + " " +
        Me.dg_food.Rows(e.RowIndex).Cells("size").Value.ToString + " " +
        Me.dg_food.Rows(e.RowIndex).Cells("unit").Value.ToString + " " +
        Me.dg_food.Rows(e.RowIndex).Cells("price").Value.ToString
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        lab_times.Text = Now.ToShortTimeString
    End Sub

    Private Sub Cbx_tables_Leave(sender As Object, e As EventArgs) Handles cbx_tables.Leave
        Try
            dt_orders.Clear()
            dt_orders = conOrders.loadData("SELECT [dbo].[tb_orders].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
                [dbo].[tb_foods].[price] As price,[dbo].[tb_unit].[name] As unit,[dbo].[tb_orders].[qty] As qty,[dbo].[tb_orders].[datetime] As datetime
                FROM [dbo].[tb_orders] 
                INNER JOIN [dbo].[tb_foods] ON [dbo].[tb_foods].[Id] = [dbo].[tb_orders].[foodid] 
                INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
                WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0")
            Me.dg_ordersfood.DataSource = dt_orders
            Me.dg_ordersfood.Columns.Item("Id").HeaderText = "ລະຫັດ"
            Me.dg_ordersfood.Columns.Item("name").HeaderText = "ຊື່"
            Me.dg_ordersfood.Columns.Item("size").HeaderText = "ຂະໜາດ"
            Me.dg_ordersfood.Columns.Item("price").HeaderText = "ລາຄາ"
            Me.dg_ordersfood.Columns.Item("unit").HeaderText = "ຫົວໜ່ວຍ"
            Me.dg_ordersfood.Columns.Item("qty").HeaderText = "ຈຳນວນ"
            Me.dg_ordersfood.Columns.Item("datetime").HeaderText = "ເວລາສັ່ງ"
            dt_total.Clear()
            dt_total = conTotal.loadData("SELECT dbo.tb_foods.Id,dbo.tb_foods.name, dbo.tb_foods.size, dbo.tb_unit.name AS unit, dbo.tb_foods.price, SUM(dbo.tb_orders.qty) AS qty,
                                             dbo.tb_foods.price * SUM(dbo.tb_orders.qty) AS total,MAX(DISTINCT dbo.tb_orders.datetime) AS datetime 
                                    FROM dbo.tb_orders 
                                    INNER JOIN dbo.tb_foods ON dbo.tb_orders.foodid = dbo.tb_foods.Id INNER JOIN
                                               dbo.tb_unit ON dbo.tb_foods.unitid = dbo.tb_unit.Id
                                    WHERE [dbo].[tb_orders].[tableid]='" + cbx_tables.SelectedValue.ToString + "' AND [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0 
                                    GROUP BY dbo.tb_foods.Id, dbo.tb_foods.price, dbo.tb_foods.size, dbo.tb_foods.name, dbo.tb_unit.name")
            Me.dg_total.DataSource = dt_total
            Me.dg_total.Columns.Item("Id").HeaderText = "ລະຫັດ"
            Me.dg_total.Columns.Item("name").HeaderText = "ຊື່"
            Me.dg_total.Columns.Item("size").HeaderText = "ຂະໜາດ"
            Me.dg_total.Columns.Item("price").HeaderText = "ລາຄາ"
            Me.dg_total.Columns.Item("unit").HeaderText = "ຫົວໜ່ວຍ"
            Me.dg_total.Columns.Item("qty").HeaderText = "ຈຳນວນ"
            Me.dg_total.Columns.Item("total").HeaderText = "ລວມເປັນເງີນ"
            Me.dg_total.Columns.Item("datetime").HeaderText = "ເວລາສັ່ງລາສຸດ"
            dt_sum.Clear()
            dt_sum = conSum.loadData("SELECT SUM(dbo.v_total.total) AS Total, MAX(DISTINCT dbo.v_total.datetime) AS datetime FROM dbo.v_total WHERE [tableid]='" + cbx_tables.SelectedValue.ToString + "'")
            Me.lab_total.Text = dt_sum.Rows(0).ItemArray(0).ToString
            Me.lab_datetime.Text = dt_sum.Rows(0).ItemArray(1).ToString
            Dim items As String = ""
            Dim no As Integer = 1
            For Row As Integer = 0 To Me.dg_total.Rows.Count - 1

                items += no.ToString + ". " + Me.dg_total.Rows(Row).Cells("name").Value.ToString + " " + Me.dg_total.Rows(Row).Cells("size").Value.ToString +
                    " " + Me.dg_total.Rows(Row).Cells("unit").Value.ToString + " " + Convert.ToInt32(Me.dg_total.Rows(Row).Cells("price").Value).ToString("#,###") +
                    " x" + Me.dg_total.Rows(Row).Cells("unit").Value.ToString + " : " + Convert.ToInt32(Me.dg_total.Rows(Row).Cells("total").Value).ToString("#,###") + vbNewLine
                no += 1
            Next
            RichTextBox1.Text = "ໃບບີນ: " + txt_billno.Text.ToString + "
----------------------------------------------------------
        Made In Mekong
----------------------------------------------------------
ໂຕະ: " + cbx_tables.Text.ToString + "
----------------------------------------------------------
" + items.ToString + "
                      -------------------------------------
                                    = " + Convert.ToInt32(lab_total.Text.ToString).ToString("#,###") + "
-----------------------------------------------------------
ວັນທີ: " + lab_datetime.Text.ToString + "
-----------------------------------------------------------
       ຂອບໃຈລູກຄ້າທີ່ມາອຸດໜູນ"
        Catch ex As Exception
            MessageBox.Show("ຍັງບໍ່ມິລາຍການສັ່ງອາຫານ", "ພົບບັນຫາ")
        End Try

    End Sub

    Private Sub Txt_searchfood_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_searchfood.KeyDown
        If e.KeyCode = Keys.Enter Then
            dt_foods.Clear()
            dt = conFoods.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
            [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
            [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[Id] As type 
            FROM [dbo].[tb_foods] 
            INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid] 
            WHERE [dbo].[tb_foods].[name] LIKE N'%" + Me.txt_search.Text + "%' OR [dbo].[tb_foods].[Id] LIKE '%" + Me.txt_search.Text + "%'")
            Me.dg_food.Refresh()
        End If
    End Sub

    Private Sub Txt_search_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Tsb_refresh_Click(sender As Object, e As EventArgs) Handles tsb_refresh.Click
        load()
    End Sub

    Private Sub load()
        dt_tables.Clear()
        dt_tables = conTables.loadData("SELECT * FROM [dbo].[tb_tables] WHERE [Enabled]=1")
        Me.cbx_tables.DataSource = dt_tables
        Me.cbx_tables.DisplayMember = "name"
        Me.cbx_tables.ValueMember = "Id"
        dt_tablechange.Clear()
        dt_tablechange = conTableschange.loadData("SELECT dbo.tb_tables.Id, dbo.tb_tables.name
                                                FROM dbo.tb_tables 
                                                WHERE (dbo.tb_tables.status = 1)")
        Me.cbx_changetables.DataSource = dt_tablechange
        Me.cbx_changetables.DisplayMember = "name"
        Me.cbx_changetables.ValueMember = "Id"
        dtbill.Clear()
        dtbill = conBill.loadData("Select IDENT_CURRENT('tb_transactions') AS Current_Identity;")
        billnumber = 1000 + CInt(dt.Rows(0).ItemArray(0).ToString)
        txt_billno.Text = "MM" + billnumber.ToString
        dt_foods.Clear()
        dt_foods = conFoods.loadData("SELECT [dbo].[tb_foods].[Id] As Id, [dbo].[tb_foods].[name] As name, [dbo].[tb_foods].[size] As size,
        [dbo].[tb_foods].[price] As price, [dbo].[tb_foods].[unitid] As unitid, [dbo].[tb_foods].[typeid] As typeid,
        [dbo].[tb_unit].[name] As unit, [dbo].[tb_types].[name] As type 
        FROM [dbo].[tb_foods] 
        INNER JOIN [dbo].[tb_unit] ON [dbo].[tb_unit].[Id] = [dbo].[tb_foods].[unitid] 
        INNER JOIN [dbo].[tb_types] ON [dbo].[tb_types].[Id] = [dbo].[tb_foods].[typeid]")
        Me.dg_food.DataSource = dt_foods
        Me.dg_food.Columns.Item("Id").HeaderText = "ລະຫັດ"
        Me.dg_food.Columns.Item("name").HeaderText = "ຊື່"
        Me.dg_food.Columns.Item("size").HeaderText = "ຂະໜາດ"
        Me.dg_food.Columns.Item("price").HeaderText = "ລາຄາ"
        Me.dg_food.Columns.Item("unit").HeaderText = "ຫົວໜ່ວຍ"
        Me.dg_food.Columns.Item("type").HeaderText = "ປະເພດ"
        Me.dg_food.Columns.Item("unitid").Visible = False
        Me.dg_food.Columns.Item("typeid").Visible = False
        dt_tablestate.Clear()
        dt_tablestate = conTablestate.loadData("SELECT dbo.tb_tables.Id, dbo.tb_tables.name
                                                FROM dbo.tb_tables 
                                                WHERE (dbo.tb_tables.status = 0)")
        Me.dg_tablestate.DataSource = dt_tablestate
        Me.dg_tablestate.Columns.Item("Id").HeaderText = "ລະຫັດ"
        Me.dg_tablestate.Columns.Item("name").HeaderText = "ຊື່"
    End Sub

    Private Sub Tsb_lock_Click(sender As Object, e As EventArgs) Handles tsb_lock.Click
        login.Show()
        Me.Close()
    End Sub

    Private Sub Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        userid = login.id
        dt = clCon.loadData("SELECT Id,username,password,name,status,enabled FROM tb_users Where Id=" + userid.ToString)
        username = dt.Rows(0).ItemArray(3).ToString()
        Me.tsb_username.Text = username
        load()
    End Sub

    Private Sub Tsb_types_Click(sender As Object, e As EventArgs) Handles tsb_types.Click
        types.Show()
    End Sub

    Private Sub Tsb_unit_Click(sender As Object, e As EventArgs) Handles tsb_unit.Click
        unit.Show()
    End Sub

    Private Sub Tsb_tables_Click(sender As Object, e As EventArgs) Handles tsb_tables.Click
        tables.Show()
    End Sub

    Private Sub Tbs_employees_Click(sender As Object, e As EventArgs) Handles tbs_employees.Click
        employees.Show()
    End Sub

    Private Sub Tsb_stocks_Click(sender As Object, e As EventArgs) Handles tsb_stocks.Click
        stocks.Show()
    End Sub

    Private Sub Tsb_reportin_Click(sender As Object, e As EventArgs) Handles tsb_reportin.Click
        report_in.Show()
    End Sub

    Private Sub Tsb_reportstocks_Click(sender As Object, e As EventArgs) Handles tsb_reportstocks.Click
        report_stocks.Show()
    End Sub

    Private Sub Tsb_food_Click(sender As Object, e As EventArgs) Handles tsb_food.Click
        foods.Show()
    End Sub

End Class