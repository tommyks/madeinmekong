﻿'Imports System.Data.SqlClient to use SqlClient
Imports System.Data.SqlClient
'Class ສຳລັບສ້າງ Function ໃນການຕໍ່ SQL server ແລະ ອື່ນໆ 
Public Class ClConnections
    'Create a Connection object
    Private myConn As SqlConnection = New SqlConnection("Data Source=.;Initial Catalog=myDb;Integrated Security=True")
    'Create ADO.NET objects
    Private myCmd As New SqlCommand
    Private myReader As SqlDataReader
    Private myAdapter As New SqlDataAdapter
    Private ds As New DataSet
    Private id As Integer
    Public Function storeData(ByVal query As String)
        'Create a Command object
        myCmd = myConn.CreateCommand
        myCmd.CommandText = query
        'Try Block error
        Try
            'Open the connection
            myConn.Open()
            myCmd.ExecuteNonQuery()
            myConn.Close()
            Return True
        Catch ex As Exception
            Console.Out.WriteLine(ex.ToString)
            Return False
        End Try
    End Function
    Public Function loadData(ByVal query As String) As DataTable
        'Create a Command object
        myCmd = myConn.CreateCommand
        myCmd.CommandText = query
        'Try Block error
        Try
            'Open the connection
            myConn.Open()
            'Create a Adapter
            myAdapter.SelectCommand = myCmd
            'Fill Dataset
            myAdapter.Fill(ds)
            myConn.Close()
        Catch ex As Exception
            Console.Out.WriteLine(ex.ToString)
        End Try
        'Return DataTable
        Return ds.Tables(0)
    End Function
    Private Function intval(ByVal query As String)
        'Create a Command object
        myCmd = myConn.CreateCommand
        myCmd.CommandText = query
        'Open the connection
        myConn.Open()
        myReader = myCmd.ExecuteReader()
        Try
            If myReader.Read() Then
                'If 
                'MsgBox("Password is matched")
                id = myReader.GetInt32(0) 'set id return to use our form
                Return id
            Else
                'condition with  no results
                'MsgBox("Password is not matched")
                Return False
            End If
        Finally
            'Close the reader and the database connection
            myReader.Close()
            myConn.Close()
        End Try
    End Function
    Public Function login(ByVal username As String, ByVal password As String)
        'use function intval to check login and get userID
        Dim rs As Integer = Me.intval("SELECT Id,username,password,name,status,enabled FROM tb_users Where enabled=1 and username='" + username + "' and password='" + password + "'")
        Return rs ' return userID to form
    End Function

End Class
