﻿Public Class unit
    Private clCon As New ClConnections
    Private dt, dtcbx As New DataTable
    Dim dataId As String
    Dim edit As Boolean

    Private Sub Unit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dt = clCon.loadData("SELECT [dbo].[tb_unit].[Id] As Id, [dbo].[tb_unit].[name] As unit, [dbo].[tb_unit].[typeid] As typsid, [dbo].[tb_types].[name] As types, [dbo].[tb_types].[stocks] As stocks  
                            FROM [dbo].[tb_unit] 
                            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_unit].[typeid] = [dbo].[tb_types].[Id]")
        Me.dg_data.DataSource = dt
        Me.dg_data.Columns.Item("Id").HeaderText = "ລະຫັດ"
        Me.dg_data.Columns.Item("unit").HeaderText = "ຫົວໜ່ວຍ"
        Me.dg_data.Columns.Item("unit").Width = 200
        Me.dg_data.Columns.Item("typsid").Visible = False
        Me.dg_data.Columns.Item("types").HeaderText = "ປະເພດ"
        Me.dg_data.Columns.Item("types").Width = 180
        Me.dg_data.Columns.Item("stocks").HeaderText = "ປະເພດໃນສາງ"
        Dim clcn As New ClConnections
        dtcbx = clcn.loadData("SELECT * FROM tb_types")
        Me.cbx_types.DataSource = dtcbx
        Me.cbx_types.DisplayMember = "name"
        Me.cbx_types.ValueMember = "Id"
    End Sub

    Private Sub Tsb_add_Click(sender As Object, e As EventArgs) Handles tsb_add.Click
        edit = False
        gb_add.Text = "ເພິ່ມຫົວໜ່ວຍ"
        cbx_types.SelectedIndex = 0
        txt_unitname.Clear()
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_edit_Click(sender As Object, e As EventArgs) Handles tsb_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂຫົວໜ່ວຍ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_del_Click(sender As Object, e As EventArgs) Handles tsb_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບຫົວໜ່ວຍ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_unit] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_unit].[Id] As Id, [dbo].[tb_unit].[name] As unit, [dbo].[tb_unit].[typeid] As typsid, [dbo].[tb_types].[name] As types, [dbo].[tb_types].[stocks] As stocks 
                            FROM [dbo].[tb_unit] 
                            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_unit].[typeid] = [dbo].[tb_types].[Id]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Tsm_edit_Click(sender As Object, e As EventArgs) Handles tsm_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂຫົວໜ່ວຍ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsm_del_Click(sender As Object, e As EventArgs) Handles tsm_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບຫົວໜ່ວຍ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_unit] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_unit].[Id] As Id, [dbo].[tb_unit].[name] As unit, [dbo].[tb_unit].[typeid] As typsid, [dbo].[tb_types].[name] As types, [dbo].[tb_types].[stocks] As stocks 
                            FROM [dbo].[tb_unit] 
                            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_unit].[typeid] = [dbo].[tb_types].[Id]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Btn_store_Click(sender As Object, e As EventArgs) Handles btn_store.Click
        Dim ck As Integer = 0
        If edit = True Then
            Dim rs As Boolean = clCon.storeData("UPDATE [dbo].[tb_unit] SET [name]=N'" + txt_unitname.Text + "',[typeid]='" + cbx_types.SelectedValue.ToString + "' WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_unit].[Id] As Id, [dbo].[tb_unit].[name] As unit, [dbo].[tb_unit].[typeid] As typsid, [dbo].[tb_types].[name] As types, [dbo].[tb_types].[stocks] As stocks 
                            FROM [dbo].[tb_unit] 
                            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_unit].[typeid] = [dbo].[tb_types].[Id]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ປັບປຸງຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການປັບປຸງຂໍ້ມູນ"
            End If
        Else
            Dim rs As Boolean = clCon.storeData("INSERT INTO [dbo].[tb_unit] ([name], [typeid]) VALUES (N'" + txt_unitname.Text + "', '" + cbx_types.SelectedValue.ToString + "')")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT [dbo].[tb_unit].[Id] As Id, [dbo].[tb_unit].[name] As unit, [dbo].[tb_unit].[typeid] As typsid, [dbo].[tb_types].[name] As types, [dbo].[tb_types].[stocks] As stocks 
                            FROM [dbo].[tb_unit] 
                            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_unit].[typeid] = [dbo].[tb_types].[Id]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ເພິ່ມຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການເພິ່ມຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Btn_closeadd_Click(sender As Object, e As EventArgs) Handles btn_closeadd.Click
        gb_add.Visible = False
    End Sub

    Private Sub Dg_data_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_data.CellEnter
        Dim id = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        dataId = id
        If edit = True Then
            Me.txt_unitname.Text = Me.dg_data.Rows(e.RowIndex).Cells(1).Value
            Me.cbx_types.SelectedValue = Me.dg_data.Rows(e.RowIndex).Cells("typsid").Value
        End If
        Me.tsl_id.Text = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        Me.tsl_status.Text = ""
    End Sub

    Private Sub Txt_search_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            dt.Clear()
            dt = clCon.loadData("SELECT [dbo].[tb_unit].[Id] As Id, [dbo].[tb_unit].[name] As unit, [dbo].[tb_unit].[typeid] As typsid, [dbo].[tb_types].[name] As types, [dbo].[tb_types].[stocks] As stocks 
            FROM [dbo].[tb_unit] 
            INNER JOIN [dbo].[tb_types] ON [dbo].[tb_unit].[typeid] = [dbo].[tb_types].[Id] 
            WHERE [dbo].[tb_unit].[name] LIKE N'%" + Me.txt_search.Text + "%' OR [dbo].[tb_unit].[Id] LIKE '%" + Me.txt_search.Text + "%'")
            Me.dg_data.Refresh()
        End If
    End Sub

    Private Sub Chk_stocks_CheckedChanged(sender As Object, e As EventArgs) Handles chk_stocks.CheckedChanged
        Dim ck As Integer = 0
        If chk_stocks.Checked = True Then
            ck = 1
        End If
        Dim clcn As New ClConnections
        dtcbx = clcn.loadData("SELECT * FROM [dbo].[tb_types] WHERE [stocks]='" + ck.ToString + "'")
        Me.cbx_types.DataSource = dtcbx
        Me.cbx_types.DisplayMember = "name"
        Me.cbx_types.ValueMember = "Id"
    End Sub

    Private Sub Tsb_close_Click(sender As Object, e As EventArgs) Handles tsb_close.Click
        Me.Close()
    End Sub


End Class