﻿Public Class employees
    Private clCon As New ClConnections
    Private dt As New DataTable
    Dim dataId As String
    Dim edit As Boolean
    Private Sub Employees_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dt.Clear()
        dt = clCon.loadData("SELECT * FROM [dbo].[tb_employees]")
        Me.dg_data.DataSource = dt
        Me.dg_data.Columns.Item("Id").HeaderText = "ລະຫັດ"
        Me.dg_data.Columns.Item("name").HeaderText = "ຊື່"
        Me.dg_data.Columns.Item("name").Width = 100
        Me.dg_data.Columns.Item("lname").HeaderText = "ນາມສະກຸນ"
        Me.dg_data.Columns.Item("lname").Width = 100
        Me.dg_data.Columns.Item("tel").HeaderText = "ເບີໂທລະສັບ"
        Me.dg_data.Columns.Item("email").HeaderText = "ອີເມວ"
        Me.dg_data.Columns.Item("address").HeaderText = "ທີ່ຢູ່"
        Me.dg_data.Columns.Item("persontypes").HeaderText = "ປະເພດບັດ"
        Me.dg_data.Columns.Item("personid").HeaderText = "ເລກບັດ"
        Me.dg_data.Columns.Item("emergency").HeaderText = "ເບີໂທສຸກເສີນ"
        Me.dg_data.Columns.Item("userid").HeaderText = "ລະຫັດຜູ້ໃຊ້"
        Me.dg_data.Columns.Item("userid").Visible = False
        Me.cbx_persontypes.DisplayMember = "Text"
        Me.cbx_persontypes.ValueMember = "Value"
        Me.cbx_persontypes.Items.Add(New With {.Text = "ID Crad", .Value = 0})
        Me.cbx_persontypes.Items.Add(New With {.Text = "Passport", .Value = 1})
    End Sub
    Private Sub Tsb_add_Click(sender As Object, e As EventArgs) Handles tsb_add.Click
        edit = False
        gb_add.Text = "ເພິ່ມພະນັກງານ"
        Me.txt_name.Clear()
        Me.txt_lname.Clear()
        Me.txt_bd.Clear()
        Me.txt_tel.Clear()
        Me.txt_email.Clear()
        Me.txt_address.Clear()
        Me.cbx_persontypes.SelectedIndex = 0
        Me.txt_personid.Clear()
        Me.txt_emergency.Clear()
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_edit_Click(sender As Object, e As EventArgs) Handles tsb_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂພະນັກງານ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsm_edit_Click(sender As Object, e As EventArgs) Handles tsm_edit.Click
        edit = True
        gb_add.Text = "ແກ້ໄຂພະນັກງານ"
        gb_add.Visible = True
    End Sub

    Private Sub Tsb_del_Click(sender As Object, e As EventArgs) Handles tsb_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບຂໍ້ມູນພະນັກງານ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_employees] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_employees]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Tsm_del_Click(sender As Object, e As EventArgs) Handles tsm_del.Click
        Dim result As Integer = MessageBox.Show("ຕ້ອງການລຶບຂໍ້ມູນລະຫັດ: " + dataId + " ນີ້ບໍ່?", "ລຶບຂໍ້ມູນພະນັກງານ", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim rs As Boolean = clCon.storeData("DELETE [dbo].[tb_employees] WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_employees]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ລຶບຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການລຶບຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Txt_search_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            dt.Clear()
            dt = clCon.loadData("SELECT * FROM [dbo].[tb_employees] 
            WHERE [name] LIKE N'%" + Me.txt_search.Text + "%' 
            OR [lname] LIKE N'%" + Me.txt_search.Text + "%'
            OR [tel] LIKE N'%" + Me.txt_search.Text + "%'
            OR [address] LIKE N'%" + Me.txt_search.Text + "%'
            OR [personid] LIKE N'%" + Me.txt_search.Text + "%'
            OR [email] LIKE N'%" + Me.txt_search.Text + "%'
            OR [emergency] LIKE N'%" + Me.txt_search.Text + "%'
            OR [Id] LIKE '%" + Me.txt_search.Text + "%'")
            Me.dg_data.Refresh()
        End If
    End Sub

    Private Sub Tsb_close_Click(sender As Object, e As EventArgs) Handles tsb_close.Click
        Me.Close()
    End Sub

    Private Sub Btn_store_Click(sender As Object, e As EventArgs) Handles btn_store.Click
        If edit = True Then
            Dim rs As Boolean = clCon.storeData("UPDATE [dbo].[tb_employees] SET 
            [name]=N'" + txt_name.Text + "', [lname]=N'" + txt_lname.Text + "', [bd]='" + txt_bd.Text.ToString + "',
            [tel]='" + txt_tel.Text + "', [email]='" + txt_email.Text + "', 
            [address]=N'" + txt_address.Text + "', [persontypes]='" + cbx_persontypes.SelectedItem.Value.ToString + "', 
            [personid]='" + txt_personid.Text + "', [emergency]='" + txt_emergency.Text + "'
            WHERE [Id] ='" + dataId + "'")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_employees]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ປັບປຸງຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການປັບປຸງຂໍ້ມູນ"
            End If
        Else
            Dim rs As Boolean = clCon.storeData("INSERT INTO [dbo].[tb_employees] 
                ([name], [lname], [bd], [tel], [email], [address], [persontypes], [personid], [emergency])
                VALUES (N'" + txt_name.Text + "', N'" + txt_lname.Text + "', '" + txt_bd.Text.ToString + "', '" + txt_tel.Text + "', N'" + txt_email.Text + "', N'" +
                txt_address.Text + "', '" + cbx_persontypes.SelectedItem.Value.ToString + "', '" + txt_personid.Text + "', '" + txt_emergency.Text + "')")
            If rs = True Then
                dt.Clear()
                dt = clCon.loadData("SELECT * FROM [dbo].[tb_employees]")
                Me.dg_data.Refresh()
                Me.tsl_status.ForeColor = Color.Blue
                Me.tsl_status.Text = "ເພິ່ມຂໍ້ມູນສຳເລັດ"
            Else
                Me.tsl_status.ForeColor = Color.Red
                Me.tsl_status.Text = "ຜິດພາດໃນການເພິ່ມຂໍ້ມູນ"
            End If
        End If
    End Sub

    Private Sub Btn_closeadd_Click(sender As Object, e As EventArgs) Handles btn_closeadd.Click
        gb_add.Visible = False
    End Sub

    Private Sub Dg_data_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_data.CellEnter
        Dim id = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        dataId = id
        Me.tsl_id.Text = Me.dg_data.Rows(e.RowIndex).Cells(0).Value
        If edit = True Then
            Me.txt_name.Text = Me.dg_data.Rows(e.RowIndex).Cells("name").Value
            Me.txt_lname.Text = Me.dg_data.Rows(e.RowIndex).Cells("lname").Value
            Me.txt_bd.Text = Me.dg_data.Rows(e.RowIndex).Cells("bd").Value
            Me.txt_tel.Text = Me.dg_data.Rows(e.RowIndex).Cells("tel").Value
            Me.txt_email.Text = Me.dg_data.Rows(e.RowIndex).Cells("email").Value
            Me.txt_address.Text = Me.dg_data.Rows(e.RowIndex).Cells("address").Value
            Me.cbx_persontypes.SelectedIndex = Me.dg_data.Rows(e.RowIndex).Cells("persontypes").Value
            Me.txt_personid.Text = Me.dg_data.Rows(e.RowIndex).Cells("personid").Value
            Me.txt_emergency.Text = Me.dg_data.Rows(e.RowIndex).Cells("emergency").Value
        End If
        Me.tsl_status.Text = ""
    End Sub

End Class