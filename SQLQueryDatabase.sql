﻿/*Database Name MyDb*/
CREATE TABLE [dbo].[tb_users] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [username] CHAR (18)     NOT NULL,
    [password] NVARCHAR (50) DEFAULT ((123456)) NOT NULL,
    [name]     NVARCHAR (50) DEFAULT ('Your Name') NOT NULL,
    [status]   INT           DEFAULT ((1)) NOT NULL,
    [enabled]  BIT           DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

INSERT INTO [dbo].[tb_users] ([username], [password], [name], [status], [enabled]) VALUES (N'admin', N'1', N'Your Name', 1, 1);

CREATE TABLE [dbo].[tb_employees] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [name]        NTEXT         NOT NULL,
    [lname]       NTEXT         NULL,
    [bd]          DATE          NOT NULL,
    [tel]         TEXT          NOT NULL,
    [email]       TEXT          NULL,
    [address]     NTEXT         NULL,
    [persontypes] INT           NULL,
    [personid]    NVARCHAR (50) NULL,
    [emergency]   TEXT          NULL,
    [userid]      INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([userid]) REFERENCES [dbo].[tb_users] ([Id])
);

CREATE TABLE [dbo].[tb_types] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [name]   NVARCHAR (50) NOT NULL,
    [stocks] BIT           DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[tb_unit] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [name]   NVARCHAR (50) NOT NULL,
    [typeid] INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([typeid]) REFERENCES [dbo].[tb_types] ([Id])
);

CREATE TABLE [dbo].[tb_tables] (
    [Id]      INT           IDENTITY (1, 1) NOT NULL,
    [name]    NVARCHAR (50) NOT NULL,
    [status]  BIT           DEFAULT ((1)) NOT NULL,
    [enabled] BIT           DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[tb_foods] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [name]   NVARCHAR (50) NOT NULL,
    [size]   NVARCHAR (50) NOT NULL,
    [price]  FLOAT (53)    NOT NULL,
    [unitid] INT           NOT NULL,
    [typeid] INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([unitid]) REFERENCES [dbo].[tb_unit] ([Id]),
    FOREIGN KEY ([typeid]) REFERENCES [dbo].[tb_types] ([Id])
);

CREATE TABLE [dbo].[tb_orders] (
    [Id]            INT      IDENTITY (1, 1) NOT NULL,
    [no]            NTEXT    NULL,
    [qty]           INT      NOT NULL,
    [foodid]        INT      NOT NULL,
    [tableid]       INT      NULL,
    [transactionid] INT      NULL,
    [datetime]      DATETIME NULL,
    [cancel]        BIT      DEFAULT ((0)) NULL,
    [status]        BIT      DEFAULT ((1)) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([tableid]) REFERENCES [dbo].[tb_tables] ([Id]),
    FOREIGN KEY ([transactionid]) REFERENCES [dbo].[tb_transactions] ([Id]),
    FOREIGN KEY ([foodid]) REFERENCES [dbo].[tb_foods] ([Id])
);

CREATE TABLE [dbo].[tb_transactions] (
    [Id]       INT        IDENTITY (1, 1) NOT NULL,
    [no]       NTEXT      NOT NULL,
    [amount]   FLOAT (53) NOT NULL,
    [datetime] DATETIME   NOT NULL,
    [cancel]   BIT        DEFAULT ((0)) NOT NULL,
    [payed]    BIT        DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[tb_stocks] (
    [Id]        INT      IDENTITY (1, 1) NOT NULL,
    [stockno]   NTEXT    NULL,
    [stockname] NTEXT    NOT NULL,
    [qty]       INT      NOT NULL,
    [stock]     BIT      DEFAULT ((0)) NOT NULL,
    [unitid]    INT      NOT NULL,
    [typeid]    INT      NOT NULL,
    [enabled]   BIT      DEFAULT ((1)) NOT NULL,
    [status]    INT      DEFAULT ((1)) NOT NULL,
    [datetime]  DATETIME NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([unitid]) REFERENCES [dbo].[tb_unit] ([Id]),
    FOREIGN KEY ([typeid]) REFERENCES [dbo].[tb_types] ([Id])
);

CREATE VIEW [dbo].[v_unit]
	AS SELECT [dbo].[tb_unit].[Id] As Id, [dbo].[tb_unit].[name] As unit, [dbo].[tb_unit].[typeid] As typsid, [dbo].[tb_types].[name] As type,
	[dbo].[tb_types].[stocks] As stocks 
                            FROM [dbo].[tb_unit] 
                            JOIN [dbo].[tb_types] ON [dbo].[tb_unit].[typeid] = [dbo].[tb_types].[Id]

CREATE VIEW dbo.v_total
AS
SELECT        dbo.tb_orders.tableid, dbo.tb_foods.Id,dbo.tb_foods.name, dbo.tb_foods.size, dbo.tb_unit.name AS unit, dbo.tb_foods.price, SUM(dbo.tb_orders.qty) AS qty, dbo.tb_foods.price * SUM(dbo.tb_orders.qty) AS total, MAX(DISTINCT dbo.tb_orders.datetime) 
                         AS datetime
FROM            dbo.tb_orders INNER JOIN
                         dbo.tb_foods ON dbo.tb_orders.foodid = dbo.tb_foods.Id INNER JOIN
                         dbo.tb_unit ON dbo.tb_foods.unitid = dbo.tb_unit.Id
WHERE [dbo].[tb_orders].[status]=1 AND [dbo].[tb_orders].[cancel]=0
GROUP BY dbo.tb_foods.Id, dbo.tb_foods.price, dbo.tb_foods.size, dbo.tb_foods.name, dbo.tb_unit.name, dbo.tb_orders.tableid

CREATE VIEW dbo.v_stocks
AS
SELECT        dbo.tb_stocks.Id, dbo.tb_stocks.stockno, dbo.tb_stocks.stockname, dbo.tb_stocks.qty, dbo.tb_stocks.stock, dbo.tb_stocks.unitid, dbo.tb_stocks.typeid, dbo.tb_unit.name AS unit, dbo.tb_types.name AS type, 
                         dbo.tb_stocks.enabled, dbo.tb_stocks.status, dbo.tb_stocks.datetime
FROM            dbo.tb_stocks INNER JOIN
                         dbo.tb_types ON dbo.tb_stocks.typeid = dbo.tb_types.Id INNER JOIN
                         dbo.tb_unit ON dbo.tb_stocks.unitid = dbo.tb_unit.Id

