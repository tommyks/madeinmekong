﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class stocks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(stocks))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gb_add = New System.Windows.Forms.GroupBox()
        Me.cbx_unit = New System.Windows.Forms.ComboBox()
        Me.cbx_stock = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_qty = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbx_type = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_closeadd = New System.Windows.Forms.Button()
        Me.btn_store = New System.Windows.Forms.Button()
        Me.txt_no = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsl_id = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txt_search = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsb_del = New System.Windows.Forms.ToolStripButton()
        Me.tsb_edit = New System.Windows.Forms.ToolStripButton()
        Me.tsb_add = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.tsm_del = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_edit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ctm_dg = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.dg_data = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsl_status = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gb_add.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.ctm_dg.SuspendLayout()
        CType(Me.dg_data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gb_add
        '
        Me.gb_add.Controls.Add(Me.cbx_unit)
        Me.gb_add.Controls.Add(Me.cbx_stock)
        Me.gb_add.Controls.Add(Me.Label9)
        Me.gb_add.Controls.Add(Me.txt_qty)
        Me.gb_add.Controls.Add(Me.Label8)
        Me.gb_add.Controls.Add(Me.cbx_type)
        Me.gb_add.Controls.Add(Me.Label4)
        Me.gb_add.Controls.Add(Me.Label3)
        Me.gb_add.Controls.Add(Me.txt_name)
        Me.gb_add.Controls.Add(Me.Label2)
        Me.gb_add.Controls.Add(Me.btn_closeadd)
        Me.gb_add.Controls.Add(Me.btn_store)
        Me.gb_add.Controls.Add(Me.txt_no)
        Me.gb_add.Controls.Add(Me.Label1)
        Me.gb_add.Location = New System.Drawing.Point(412, 313)
        Me.gb_add.Name = "gb_add"
        Me.gb_add.Size = New System.Drawing.Size(369, 247)
        Me.gb_add.TabIndex = 10
        Me.gb_add.TabStop = False
        Me.gb_add.Text = "ນຳເຂົ້າ-ອອກສ່າງສິນຄ້າ"
        Me.gb_add.Visible = False
        '
        'cbx_unit
        '
        Me.cbx_unit.FormattingEnabled = True
        Me.cbx_unit.Location = New System.Drawing.Point(190, 173)
        Me.cbx_unit.Name = "cbx_unit"
        Me.cbx_unit.Size = New System.Drawing.Size(170, 31)
        Me.cbx_unit.TabIndex = 5
        '
        'cbx_stock
        '
        Me.cbx_stock.FormattingEnabled = True
        Me.cbx_stock.Location = New System.Drawing.Point(10, 173)
        Me.cbx_stock.Name = "cbx_stock"
        Me.cbx_stock.Size = New System.Drawing.Size(170, 31)
        Me.cbx_stock.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(186, 87)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 23)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "ຈຳນວນ"
        '
        'txt_qty
        '
        Me.txt_qty.Location = New System.Drawing.Point(190, 113)
        Me.txt_qty.Name = "txt_qty"
        Me.txt_qty.Size = New System.Drawing.Size(170, 31)
        Me.txt_qty.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 27)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 23)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "ປະເພດ"
        '
        'cbx_type
        '
        Me.cbx_type.FormattingEnabled = True
        Me.cbx_type.Location = New System.Drawing.Point(10, 53)
        Me.cbx_type.Name = "cbx_type"
        Me.cbx_type.Size = New System.Drawing.Size(170, 31)
        Me.cbx_type.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(186, 147)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 23)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "ຫົວໜ່ວຍ"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 147)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 23)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "ເຂົ້າ/ອອກ"
        '
        'txt_name
        '
        Me.txt_name.Location = New System.Drawing.Point(10, 113)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.Size = New System.Drawing.Size(170, 31)
        Me.txt_name.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 23)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "ຊື່"
        '
        'btn_closeadd
        '
        Me.btn_closeadd.Location = New System.Drawing.Point(300, 210)
        Me.btn_closeadd.Name = "btn_closeadd"
        Me.btn_closeadd.Size = New System.Drawing.Size(60, 31)
        Me.btn_closeadd.TabIndex = 7
        Me.btn_closeadd.Text = "ປິດ"
        Me.btn_closeadd.UseVisualStyleBackColor = True
        '
        'btn_store
        '
        Me.btn_store.Location = New System.Drawing.Point(190, 210)
        Me.btn_store.Name = "btn_store"
        Me.btn_store.Size = New System.Drawing.Size(104, 31)
        Me.btn_store.TabIndex = 6
        Me.btn_store.Text = "ບັນທຶກ"
        Me.btn_store.UseVisualStyleBackColor = True
        '
        'txt_no
        '
        Me.txt_no.Location = New System.Drawing.Point(190, 53)
        Me.txt_no.Name = "txt_no"
        Me.txt_no.Size = New System.Drawing.Size(170, 31)
        Me.txt_no.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(186, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ເລກທີ"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(62, 23)
        Me.ToolStripStatusLabel1.Text = "ສະຖານະ:"
        '
        'tsl_id
        '
        Me.tsl_id.Name = "tsl_id"
        Me.tsl_id.Size = New System.Drawing.Size(0, 23)
        '
        'txt_search
        '
        Me.txt_search.AutoSize = False
        Me.txt_search.Name = "txt_search"
        Me.txt_search.Size = New System.Drawing.Size(250, 32)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 32)
        '
        'tsb_del
        '
        Me.tsb_del.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_del.Image = Global.madeinmekong.My.Resources.Resources.baseline_cancel_black_18dp
        Me.tsb_del.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_del.Name = "tsb_del"
        Me.tsb_del.Size = New System.Drawing.Size(26, 29)
        Me.tsb_del.Text = "ToolStripButton2"
        '
        'tsb_edit
        '
        Me.tsb_edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_edit.Image = Global.madeinmekong.My.Resources.Resources.baseline_edit_black_18dp
        Me.tsb_edit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_edit.Name = "tsb_edit"
        Me.tsb_edit.Size = New System.Drawing.Size(26, 29)
        Me.tsb_edit.Text = "ToolStripButton1"
        '
        'tsb_add
        '
        Me.tsb_add.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_add.Image = CType(resources.GetObject("tsb_add.Image"), System.Drawing.Image)
        Me.tsb_add.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_add.Name = "tsb_add"
        Me.tsb_add.Size = New System.Drawing.Size(26, 29)
        Me.tsb_add.Text = "add"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 32)
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(68, 29)
        Me.ToolStripLabel1.Text = "ສ່າງສິນຄ້າ"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(22, 22)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.ToolStripSeparator1, Me.tsb_add, Me.tsb_edit, Me.tsb_del, Me.ToolStripSeparator2, Me.ToolStripLabel2, Me.txt_search})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(784, 32)
        Me.ToolStrip1.TabIndex = 12
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripLabel2.Image = Global.madeinmekong.My.Resources.Resources.outline_search_black_18dp
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(22, 29)
        Me.ToolStripLabel2.Text = "ToolStripLabel2"
        '
        'tsm_del
        '
        Me.tsm_del.Font = New System.Drawing.Font("Phetsarath OT", 12.0!)
        Me.tsm_del.Image = Global.madeinmekong.My.Resources.Resources.baseline_cancel_black_18dp
        Me.tsm_del.Name = "tsm_del"
        Me.tsm_del.Size = New System.Drawing.Size(104, 28)
        Me.tsm_del.Text = "Del"
        '
        'tsm_edit
        '
        Me.tsm_edit.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsm_edit.Image = Global.madeinmekong.My.Resources.Resources.baseline_edit_black_18dp
        Me.tsm_edit.Name = "tsm_edit"
        Me.tsm_edit.Size = New System.Drawing.Size(104, 28)
        Me.tsm_edit.Text = "Edit"
        '
        'ctm_dg
        '
        Me.ctm_dg.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_edit, Me.tsm_del})
        Me.ctm_dg.Name = "ContextMenuStrip1"
        Me.ctm_dg.Size = New System.Drawing.Size(105, 60)
        '
        'dg_data
        '
        Me.dg_data.AllowUserToAddRows = False
        Me.dg_data.AllowUserToDeleteRows = False
        Me.dg_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_data.ContextMenuStrip = Me.ctm_dg
        Me.dg_data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_data.Location = New System.Drawing.Point(3, 27)
        Me.dg_data.Name = "dg_data"
        Me.dg_data.ReadOnly = True
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dg_data.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dg_data.RowTemplate.Height = 30
        Me.dg_data.Size = New System.Drawing.Size(778, 503)
        Me.dg_data.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dg_data)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(784, 533)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "ຂໍ້ມູນພະນັກງານ"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.tsl_id, Me.tsl_status})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 533)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(784, 28)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsl_status
        '
        Me.tsl_status.ForeColor = System.Drawing.Color.Red
        Me.tsl_status.Name = "tsl_status"
        Me.tsl_status.Size = New System.Drawing.Size(0, 23)
        '
        'stocks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 23.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.gb_add)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Font = New System.Drawing.Font("Phetsarath OT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "stocks"
        Me.Text = "stocks"
        Me.gb_add.ResumeLayout(False)
        Me.gb_add.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ctm_dg.ResumeLayout(False)
        CType(Me.dg_data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gb_add As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_qty As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cbx_type As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_name As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btn_closeadd As Button
    Friend WithEvents btn_store As Button
    Friend WithEvents txt_no As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents tsl_id As ToolStripStatusLabel
    Friend WithEvents txt_search As ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents tsb_del As ToolStripButton
    Friend WithEvents tsb_edit As ToolStripButton
    Friend WithEvents tsb_add As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripLabel2 As ToolStripLabel
    Friend WithEvents tsm_del As ToolStripMenuItem
    Friend WithEvents tsm_edit As ToolStripMenuItem
    Friend WithEvents ctm_dg As ContextMenuStrip
    Friend WithEvents dg_data As DataGridView
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsl_status As ToolStripStatusLabel
    Friend WithEvents cbx_stock As ComboBox
    Friend WithEvents cbx_unit As ComboBox
End Class
